//
//  achievementClassTests.swift
//  fimmigrantTests
//
//  Created by iosdev on 07/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import fimmigrant

class achievementClassTests: XCTestCase {
    
    // achievement test check
    /*
     func testAchivementInitConditionCheck() {
     let achiv = Achievement(name: "trial", exp: 300, img: "Cat", des: "Trial of the cat")
     XCTAssertTrue(achiv.completeStatus == true)
     }
     
     // achievement second test check
     func testAchivementPostInitConditionCheck() {
     let achiv = Achievement(name: "trial", exp: 300, img: "Cat", des: "Trial of the cat")
     a = 12
     
     XCTAssertTrue(achiv.completeStatus == false)
     }
     */
    // Achievement init test -not nil
    func testAchivementInitSecondTest() {
        let achiv = Achievement(name: "trial", exp: 300, img: "Cat", des: "Trial of the cat")
        XCTAssertNotNil(achiv)
    }
    
    // achievement init value test
    func testAchivementInitTest() {
        let achiv = Achievement(name: "trial", exp: 300, img: "Cat", des: "Trial of the cat")
        XCTAssertTrue(achiv.name == "trial" && achiv.expValue == 300 && achiv.image == "Cat" && achiv.desc == "Trial of the cat")
    }
    
    
}
