//
//  userClassTests.swift
//  fimmigrantTests
//
//  Created by iosdev on 26/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import fimmigrant

class userClassTests: XCTestCase {
    

    func testUserHasName() {
        let mainUser = User(name: "julianAssange", tk: "stupidXcode")
        
        
        XCTAssertTrue(mainUser.getUserName() == "julianAssange" && mainUser.getUserName().count > 2)
    }
    
    //    experience can’t be negative
    /*/func testExperienceCantBeNegative () {
     let mainUser = User(name: "julianAssange", psw: "stupidXcode")
     
     mainUser.experience = 0
     mainUser.experience = -2
     
     XCTAssertTrue(mainUser.experience > 0 && mainUser.experience == Int)
     }
     
     //   put max exp and try to add to it, Should not increase and also return false
     func testExperienceAboveMax() {
     let mainUser = User(name: "julianAssange", psw: "stupidXcode")
     
     mainUser.experience = maxExperience
     mainUser.gainExperience()
     
     XCTAssertTrue(mainUser.experience <= maxExperience && mainUser.gainExperience == false)
     }*/
    
    //    Achievements Array is empty upon init.
    func testNoAchievementsOnInit () {
        let mainUser = User(name: "julianAssange", tk: "stupidXcode")
        
        
        XCTAssertTrue(mainUser.getAchievements().count == 0)
    }
    
    //    You cannot remove an achievement, once achieved it is permanent
    func testCantRemoveAchievement () {
        let mainUser = User(name: "julianAssange", tk: "stupidXcode")
        //TODO: add achievement
        //TODO: remove achievement
        
        XCTAssertTrue(mainUser.getAchievements().count == 0)
    }
    
    //    avatarUrl is a string of at least 6 characters and contains a “.”
    /*
    func testAvatarUrlLikeUrl () {
        let mainUser = User(name: "julianAssange", tk: "stupidXcode")
        
        //TODO: set dataURL
        mainUser.setAvatarDataURL(url: "https://web.whatsapp.com/")
        
        let urlRes = mainUser.getAvatarData()
        XCTAssertTrue(mainUser.getAvatarDataURL()?.contains(".") ?? false && mainUser.getAvatarDataURL()!.count > 6 )
    }
 */
    
    //    AvatarData is nil at init
    func testAvatarDataNilOnInit () {
        let mainUser = User(name: "julianAssange", tk: "stupidXcode")
        
        XCTAssertNil(mainUser.getAvatarDataURL())
    }
    
    //    get data at init should return false
    func testGetAvatarDataNilOnInit () {
        let mainUser = User(name: "julianAssange", tk: "stupidXcode")
        
        XCTAssertNil(mainUser.getAvatarData())
    }
    
    //    set avatar data and check (get) if data is not nil anymore.
    /*func testGetDataReturnsData() {
     let mainUser = User(name: "julianAssange", psw: "stupidXcode")
     //TODO: Add some data
     
     XCTAssertNotNil(mainUser.getAvatarData())
     }*/
    
    /* tasklist inaccessible
     //    Tasklist is not empty
     func testTasklistNotEmpty () {
     let mainUser = User(name: "julianAssange", psw: "stupidXcode")
     
     XCTAssertTrue(mainUser.taskList.count > 0)
     }*/
    
    /* no levels
     //    level should be 0 on init.
     func testLevelOnInit() {
     let mainUser = User(name: "julianAssange", psw: "stupidXcode")
     
     XCTAssertTrue(mainUser.level == 0)
     }
     
     //    change to level 3 and check if it is at level 3
     func levelChangeSucceesful() {
     let mainUser = User(name: "julianAssange", psw: "stupidXcode")
     //TODO: change the level to level 3
     
     XCTAssertTrue(mainUser.level == 3)
     }*/
    
    
    
    //    checkTaskCompletion- set 3 tasks complete, does this return int 3
    func testCheckTaskCompletion() {
        //This needs help
        
    }
    
    
    var a = 5
    var b = 8
    func giveCheck() -> Bool {
        return a<b
    }
    
    
    
    func testIsAchievementAddSuccessful() {
        let mainUser = User(name: "julianAssange", tk: "stupidXcode")
        let achiv = Achievement(name: "trial", exp: 300, img: "Cat", des: "Trial of the cat")
        mainUser.addAchievements(achiv: achiv)
        XCTAssertTrue(mainUser.getAchievements()[0].name == "trial")
    }
    
    //  getname- the name is a string that is longer that is not empty
    func testGetNameFuncGivesName() {
        let mainUser = User(name: "julianAssange", tk: "stupidXcode")
        XCTAssertTrue(mainUser.getUserName() == "julianAssange")
    }
    
    //    setName- returns name that is the same as the new name you just set
    func testDidNameSet() {
        let mainUser = User(name: "julianAssange", tk: "stupidXcode")
        let _ = mainUser.setName(newName: "Carolus Linnaeus")
        
        XCTAssertTrue(mainUser.getUserName() == "Carolus Linnaeus")
    }
    
    //    addTask- is task list up by one, is that task in the task list.
    func testDidTasksIncrease() {
        
        let mainUser = User(name: "julianAssange", tk: "stupidXcode")
        
        let stepArr: [Step] = makeRandomStepArr()
        
        let theTask = Task(title: "Test task", steps: stepArr, description: "This is a test task", link: "https://gitlab.com/zerihund/fimmigrant", preq: "Use your head")
     
        let num = mainUser.taskList.count
     
        //add a test task
        mainUser.addTask(task: theTask)
        print(mainUser.taskList.count)
        
        XCTAssert(mainUser.taskList.count == 1)
    
    }
    
    func makeRandomStepArr() -> [Step] {
        var steps: [Step] = []
        
        let step1 = Step(whatToDo: "Wake up")
        let step2 = Step(whatToDo: "Go brush your teeth")
        let step3 = Step(whatToDo: "Have breakfast")
        let step4 = Step(whatToDo: "Go to school")
        
        steps.append(step1)
        steps.append(step2)
        steps.append(step3)
        steps.append(step4)
        
        return steps
    }

}
