//
//  taskClassTests.swift
//  fimmigrantTests
//
//  Created by iosdev on 07/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import fimmigrant

class taskClassTest: XCTestCase {
    
    
    func makeRandomStepArr() -> [Step] {
        var steps: [Step] = []
        
        let step1 = Step(whatToDo: "Wake up")
        let step2 = Step(whatToDo: "Go brush your teeth")
        let step3 = Step(whatToDo: "Have breakfast")
        let step4 = Step(whatToDo: "Go to school")
        
        steps.append(step1)
        steps.append(step2)
        steps.append(step3)
        steps.append(step4)
        
        return steps
    }
    
    func setSomeTasksDone(stepArr: [Step] ) {
        stepArr[0].setDone(state: true)
        stepArr[1].setDone(state: true)
    }
    
    
    func testTaskHasName() {
        let stepArr: [Step] = makeRandomStepArr()
        var theTask = Task(title: "Test task", steps: stepArr, description: "This is a test task", link: "https://gitlab.com/zerihund/fimmigrant", preq: "Use your head")
        
        XCTAssert(theTask.title == "Test task" && theTask.title.count > 5)
    }
    
    
    func testTaskHasSteps() {
        let stepArr = makeRandomStepArr()
        var theTask = Task(title: "Test task", steps: stepArr, description: "This is a test task", link: "https://gitlab.com/zerihund/fimmigrant", preq: "Use your head")
        
        
        XCTAssert(theTask.stepArray.count >= 1)
    }
    
    func testCheckHowManyDone() {//Set 2 steps as done. Tests the counter aas well as ability to set to done
        let stepArr = makeRandomStepArr()
        var theTask = Task(title: "Test task", steps: stepArr, description: "This is a test task", link: "https://gitlab.com/zerihund/fimmigrant", preq: "Use your head")
        
        setSomeTasksDone(stepArr: theTask.stepArray)
        
        XCTAssert(theTask.doneStepsCounter() == 2)
    }
    
    
    
}
