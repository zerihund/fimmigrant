//
//  stepClassTests.swift
//  fimmigrantTests
//
//  Created by iosdev on 07/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import fimmigrant

class stepClassTests: XCTestCase {
    
    func makeRandomStepArr() -> [Step] {
        var steps: [Step] = []
        
        let step1 = Step(whatToDo: "Wake up")
        let step2 = Step(whatToDo: "Go brush your teeth")
        let step3 = Step(whatToDo: "Have breakfast")
        let step4 = Step(whatToDo: "Go to school")
        
        steps.append(step1)
        steps.append(step2)
        steps.append(step3)
        steps.append(step4)
        
        return steps
    }
    
    func testSetDone() {
        let step1 = Step(whatToDo: "Wake up")
        
        step1.setDone(state: true)
        
        XCTAssert(step1.getDone() == 1 )
    }
    
    func testGetDone() {
        let step1 = Step(whatToDo: "Wake up")
        
        XCTAssertNotNil(step1.getDone())
    }
    
    //
    func testInstructionIsNotBlank () {
        let step1 = Step(whatToDo: "Wake up")
        
        XCTAssertTrue(step1.instruction.count > 5 && step1.instruction == "Wake up")
    }
    
    //step done is false on init
    func testStepIsFalseAtInit () {
        let step1 = Step(whatToDo: "Wake up")
        
        //getDone returns 0 when it's false and 1 when it's true
        XCTAssertTrue(step1.getDone() == 0)
    }
    
}
