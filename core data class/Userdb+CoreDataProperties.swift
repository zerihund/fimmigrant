//
//  Userdb+CoreDataProperties.swift
//  fimmigrant
//
//  Created by iosdev on 05/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//
//

import Foundation
import CoreData


extension Userdb {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Userdb> {
        return NSFetchRequest<Userdb>(entityName: "Userdb")
    }

    @NSManaged public var achevement: NSObject?
    @NSManaged public var avatar: String?
    @NSManaged public var experience: Int16
    @NSManaged public var lastupdate: Int16
    @NSManaged public var tasks: NSObject?
    @NSManaged public var token: String?
    @NSManaged public var username: String?
    @NSManaged public var achieve: [Achievementdb]?
    @NSManaged public var has: [Taskdb]?

}

// MARK: Generated accessors for achieve
extension Userdb {

    @objc(insertObject:inAchieveAtIndex:)
    @NSManaged public func insertIntoAchieve(_ value: Achievementdb, at idx: Int)

    @objc(removeObjectFromAchieveAtIndex:)
    @NSManaged public func removeFromAchieve(at idx: Int)

    @objc(insertAchieve:atIndexes:)
    @NSManaged public func insertIntoAchieve(_ values: [Achievementdb], at indexes: NSIndexSet)

    @objc(removeAchieveAtIndexes:)
    @NSManaged public func removeFromAchieve(at indexes: NSIndexSet)

    @objc(replaceObjectInAchieveAtIndex:withObject:)
    @NSManaged public func replaceAchieve(at idx: Int, with value: Achievementdb)

    @objc(replaceAchieveAtIndexes:withAchieve:)
    @NSManaged public func replaceAchieve(at indexes: NSIndexSet, with values: [Achievementdb])

    @objc(addAchieveObject:)
    @NSManaged public func addToAchieve(_ value: Achievementdb)

    @objc(removeAchieveObject:)
    @NSManaged public func removeFromAchieve(_ value: Achievementdb)

    @objc(addAchieve:)
    @NSManaged public func addToAchieve(_ values: NSOrderedSet)

    @objc(removeAchieve:)
    @NSManaged public func removeFromAchieve(_ values: NSOrderedSet)

}

// MARK: Generated accessors for has
extension Userdb {

    @objc(insertObject:inHasAtIndex:)
    @NSManaged public func insertIntoHas(_ value: Taskdb, at idx: Int)

    @objc(removeObjectFromHasAtIndex:)
    @NSManaged public func removeFromHas(at idx: Int)

    @objc(insertHas:atIndexes:)
    @NSManaged public func insertIntoHas(_ values: [Taskdb], at indexes: NSIndexSet)

    @objc(removeHasAtIndexes:)
    @NSManaged public func removeFromHas(at indexes: NSIndexSet)

    @objc(replaceObjectInHasAtIndex:withObject:)
    @NSManaged public func replaceHas(at idx: Int, with value: Taskdb)

    @objc(replaceHasAtIndexes:withHas:)
    @NSManaged public func replaceHas(at indexes: NSIndexSet, with values: [Taskdb])

    @objc(addHasObject:)
    @NSManaged public func addToHas(_ value: Taskdb)

    @objc(removeHasObject:)
    @NSManaged public func removeFromHas(_ value: Taskdb)

    @objc(addHas:)
    @NSManaged public func addToHas(_ values: NSOrderedSet)

    @objc(removeHas:)
    @NSManaged public func removeFromHas(_ values: NSOrderedSet)

}
