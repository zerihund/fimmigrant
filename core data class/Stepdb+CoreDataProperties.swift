//
//  Stepdb+CoreDataProperties.swift
//  fimmigrant
//
//  Created by iosdev on 26/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//
//

import Foundation
import CoreData


extension Stepdb {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Stepdb> {
        return NSFetchRequest<Stepdb>(entityName: "Stepdb")
    }

    @NSManaged public var descriptions: String?
    @NSManaged public var status: Int16
    @NSManaged public var belongsTo: Taskdb?

}
