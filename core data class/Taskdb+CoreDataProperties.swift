//
//  Taskdb+CoreDataProperties.swift
//  fimmigrant
//
//  Created by iosdev on 26/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//
//

import Foundation
import CoreData

extension Taskdb {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Taskdb> {
        return NSFetchRequest<Taskdb>(entityName: "Taskdb")
    }

    @NSManaged public var descriptions: String?
    @NSManaged public var title: String?
    @NSManaged public var link: String?
    @NSManaged public var prerequisite: String?
    @NSManaged public var contain: [Stepdb]?
    @NSManaged public var ownedBy: Userdb?
    public var progress: Int16 {
        var i: Int16 = 0
        if let stepList = contain {
            for each in stepList {
                i += each.status
            }
        }
        return i
    }
    
    public var finis: Bool {
        guard let stepList = contain else {
            return false
        }
        
        if (stepList.count == progress) {
            return true
        } else {
            return false
        }
    }
    
    public func setDone() {
        guard let stepList = contain else {
            return
        }
        for each in stepList {
            each.status = 1
        }
    }
}

// MARK: Generated accessors for contain
extension Taskdb {

    @objc(insertObject:inContainAtIndex:)
    @NSManaged public func insertIntoContain(_ value: Stepdb, at idx: Int)

    @objc(removeObjectFromContainAtIndex:)
    @NSManaged public func removeFromContain(at idx: Int)

    @objc(insertContain:atIndexes:)
    @NSManaged public func insertIntoContain(_ values: [Stepdb], at indexes: NSIndexSet)

    @objc(removeContainAtIndexes:)
    @NSManaged public func removeFromContain(at indexes: NSIndexSet)

    @objc(replaceObjectInContainAtIndex:withObject:)
    @NSManaged public func replaceContain(at idx: Int, with value: Stepdb)

    @objc(replaceContainAtIndexes:withContain:)
    @NSManaged public func replaceContain(at indexes: NSIndexSet, with values: [Stepdb])

    @objc(addContainObject:)
    @NSManaged public func addToContain(_ value: Stepdb)

    @objc(removeContainObject:)
    @NSManaged public func removeFromContain(_ value: Stepdb)

    @objc(addContain:)
    @NSManaged public func addToContain(_ values: NSOrderedSet)

    @objc(removeContain:)
    @NSManaged public func removeFromContain(_ values: NSOrderedSet)

}
