//
//  Achievementdb+CoreDataProperties.swift
//  fimmigrant
//
//  Created by iosdev on 03/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//
//

import Foundation
import CoreData


extension Achievementdb {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Achievementdb> {
        return NSFetchRequest<Achievementdb>(entityName: "Achievementdb")
    }

    @NSManaged public var desc: String?
    @NSManaged public var exp: Int32
    @NSManaged public var img: String?
    @NSManaged public var name: String?
    @NSManaged public var doneBy: Userdb?

}
