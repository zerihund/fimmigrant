//
//  AllTaskTableViewCell.swift
//  fimmigrant
//
//  Created by iosdev on 03/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class AllTaskTableViewCell: UITableViewCell {

    @IBOutlet weak var taskTitleLabel: UILabel!
    @IBOutlet weak var taskImage: UIImageView!
    
    var taskItem : Task!
    var delegate : AllTaskCellDelegate?

    
    //designates which task is going to be displayed in the cell. you pass to it the indexPath.row
    func setTaskForCell(theTask: Task){
        taskItem = theTask
        taskImage.image = assignImageToTask(theTaskName: taskItem.title)
        taskTitleLabel.text = taskItem.title
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 18, left: 18, bottom: 18, right: 18))
    }


    @IBAction func addToMyTask(_ sender: UIButton) {
        // In the delegate this function is called to add to myTask(and remove from allTasks)
        delegate?.addTask(thisTask: taskItem)
        //print("Add button clicked")
    }
    
    
    func assignImageToTask(theTaskName: String) -> UIImage? {
        let img: UIImage?
        
        switch theTaskName {
        case "Kela card":
            img = UIImage.init(named: "kelaLogo")
        case "HSL Card":
            img = UIImage.init(named: "hslLogo")
        case "General Housing Allowance":
            img = UIImage.init(named: "kelaLogo")
        case "Tax Card":
            img = UIImage.init(named: "taxLogo")
        case "Occupational Safety Card":
            img = UIImage.init(named: "safeftyCard")
        case "Hygiene Passport":
            img = UIImage.init(named: "hygienePassportLogo")
        case "Hot work license":
            img = UIImage.init(named: "hotLicence")
        case "Finnish identity card":
            img = UIImage.init(named: "poliisi_logo")
        default:
            img = UIImage.init(named: "harold")
        }
        return img
    }
}

protocol AllTaskCellDelegate {
    //adds to my myTaskS
    func addTask(thisTask: Task)
}


