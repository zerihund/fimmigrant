//
//  TaskTableViewCell.swift
//  fimmigrant
//
//  Created by iosdev on 28/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {

    @IBOutlet weak var taskImageView: UIImageView!
    @IBOutlet weak var taskTitleLabel: UILabel!
    var task: Task? = nil
    
    //designates which task is going to be displayed in the cell. you pass to it the indexPath.row
    func setTaskForCell(theTask: Task){
        self.task = theTask
        
        taskImageView.image = assignImageToTask(theTaskName: theTask.title)
        taskTitleLabel.text = theTask.title
    }
    
    func assignImageToTask(theTaskName: String) -> UIImage? {
        let img: UIImage?
        
        switch theTaskName {
        case "Kela card":
            img = UIImage.init(named: "kelaLogo")
        case "HSL Card":
            img = UIImage.init(named: "hslLogo")
        case "General Housing Allowance":
            img = UIImage.init(named: "kelaLogo")
        case "Tax Card":
            img = UIImage.init(named: "taxLogo")
        case "Occupational Safety Card":
            img = UIImage.init(named: "safeftyCard")
        case "Hygiene Passport":
            img = UIImage.init(named: "hygienePassportLogo")
        case "Hot work license":
            img = UIImage.init(named: "hotLicence")
        case "Finnish identity card":
            img = UIImage.init(named: "poliisi_logo")
        default:
            img = UIImage.init(named: "harold")
        }
        return img
    }
    
    

}
