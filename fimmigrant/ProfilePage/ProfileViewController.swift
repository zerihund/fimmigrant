//
//  ProfileViewController.swift
//  fimmigrant
//
//  Created by iosdev on 25/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    // achivment image
    @IBOutlet weak var achivOne: UIImageView!
    @IBOutlet weak var achivTwo: UIImageView!
    @IBOutlet weak var achivThree: UIImageView!
    @IBOutlet weak var achivFour: UIImageView!
    @IBOutlet weak var achivFive: UIImageView!
    @IBOutlet weak var achivSix: UIImageView!
    
    // achivment labels
    @IBOutlet weak var achivLabel1: UILabel!
    @IBOutlet weak var achivLabel2: UILabel!
    @IBOutlet weak var achivLabel3: UILabel!
    @IBOutlet weak var achivLabel4: UILabel!
    @IBOutlet weak var achivLabel5: UILabel!
    @IBOutlet weak var achivLabel6: UILabel!
    
    // other UI elements
    @IBOutlet weak var leftXP: UILabel!
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var xpAmount: UILabel!
    @IBOutlet weak var expBar: UIView!
    var thisUser: User?
    
    // width anchor
    var const: Float = 0
    var slideBar = UIView()
    //TODO: Connect Image to UI once we know what the achievements are going to be called.
    //TODO: put achievements in stack or collectionViewGrid once we have the achievement class up and running.
    //TODO: Color Change
    
    @IBOutlet weak var achivContainer: UIView!
    @IBOutlet var popOverView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        popOverView.layer.cornerRadius = 10
        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            if let user = myDelegate.user {
                thisUser = user
                print("this is user:")
                var achivIndex = 0
                let achivImgArray = [achivOne, achivTwo, achivThree, achivFour, achivFive, achivSix]
                let achivLbArray = [achivLabel1, achivLabel2, achivLabel3, achivLabel4, achivLabel5, achivLabel6]
                for item in achivLbArray {
                    item?.text = ""
                }
                for each in user.getAchievements() {
                    print(each.image)
                    achivImgArray[achivIndex]?.image = UIImage.init(named: each.image)
                    achivLbArray[achivIndex]?.text = each.name
                    achivIndex += 1
                    
                }
                self.userName.text = user.getUserName()
                print(user.experience)
                self.xpAmount.text = "Level " + String(user.getLevel())
                if let avatar = user.getAvatarDataURL() {
                    self.avatarImg.image = UIImage(named: avatar)
                }
                avatarSetup()
            }
        }
        // Do any additional setup after loading the view
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("view enter again profile")
        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            if let user = myDelegate.user {
                thisUser = user
                print("this is user:")
                var achivIndex = 0
                let achivImgArray = [achivOne, achivTwo, achivThree, achivFour, achivFive, achivSix]
                let achivLbArray = [achivLabel1, achivLabel2, achivLabel3, achivLabel4, achivLabel5, achivLabel6]
                for item in achivLbArray {
                    item?.text = ""
                }
                for each in user.getAchievements() {
                    print(each.image)
                    achivImgArray[achivIndex]?.image = UIImage.init(named: each.image)
                    achivLbArray[achivIndex]?.text = each.name
                    achivIndex += 1
                    
                }
                self.userName.text = user.getUserName()
                print(user.experience)
                self.xpAmount.text = "Level " + String(user.getLevel())
                if let avatar = user.getAvatarDataURL() {
                    self.avatarImg.image = UIImage(named: avatar)
                }
                avatarSetup()
            }
        }
    }
    // present confirmation popover when log out
    @IBAction func logout(_ sender: UIBarButtonItem) {
        print("logout tapped")
        self.view.addSubview(self.popOverView)
        popOverView.center = self.view.center
    }
    
    // cancel logging out
    @IBAction func cancelLogOut(_ sender: UIButton) {
        self.popOverView.removeFromSuperview()
    }
    
    // confirm logging out
    @IBAction func logUserOut(_ sender: UIButton) {
        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            if let user = myDelegate.user {
                CoreDataHandler.checkData(token: user.token, logInView: nil, signUpView: nil, profileView: self)
            }
        }
        
    }
    // truly log out
    func trueLogOut() {
        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            CoreDataHandler.logOut()
            myDelegate.reInitPostLogOut()
            performSegue(withIdentifier: "logoutSegue", sender: self)
        }
    }
    
    // randomize user avatar
    @IBAction func changeAvatar(_ sender: UIButton) {
        if let user = thisUser {
            let x = "av" + String(Int.random(in: 1..<9))
            user.setAvatarDataURL(url: x)
            self.avatarImg.image = UIImage.init(named: x)
        }
    }
    func avatarSetup() {
        expBar.layer.cornerRadius = 5
        achivContainer.layer.cornerRadius = 5
        if let user = thisUser {
            let top = user.experience - (user.getLevel()*1000)
            let bot = 1000
            self.leftXP.text = String(bot - top) + " left to next level"
            let percent = Double(top)/Double(bot)
            const = Float(374 * percent)
            print("------------------------------------------const value:" + String(const))
            slideBar.removeFromSuperview()
            slideBar = UIView()
            expBar.addSubview(slideBar)
            slideBar.translatesAutoresizingMaskIntoConstraints = false
            slideBar.topAnchor.constraint(equalTo: expBar.topAnchor).isActive = true
            slideBar.bottomAnchor.constraint(equalTo: expBar.bottomAnchor).isActive = true
            slideBar.leadingAnchor.constraint(equalTo: expBar.leadingAnchor).isActive = true
            slideBar.widthAnchor.constraint(equalToConstant: CGFloat(const)).isActive = true
            slideBar.backgroundColor = .yellow
            slideBar.layer.cornerRadius = 5
            slideBar.updateConstraints()
            expBar.bringSubviewToFront(userName)
            expBar.bringSubviewToFront(leftXP)
            
        }
        avatarImg.layer.cornerRadius = avatarImg.frame.size.width / 2
        avatarImg.clipsToBounds = true
        avatarImg.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        avatarImg.layer.borderWidth = 2
    }
}
