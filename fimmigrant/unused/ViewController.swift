//
//  ViewController.swift
//  fimmigrant
//
//  Created by Kristófer Ívar Knutsen on 22/04/2019.
//  Copyright © 2019 Kristófer Ívar Knutsen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    
    
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var logoLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.init(red: 135/255, green: 191/255, blue: 170/255, alpha: 1)
        setUpUIElements()
        
        
        
    }
    
    
    @IBAction func loginButton(_ sender: UIButton) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func signupButton(_ sender: UIButton) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setUpUIElements(){
        loginButton.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        loginButton.backgroundColor = red
        loginButton.layer.cornerRadius = cornerRadius
        loginButton.layer.masksToBounds = true
        loginButton.layer.borderWidth = borderWith
        loginButton.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        signUpButton.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        signUpButton.backgroundColor = red
        signUpButton.layer.cornerRadius = cornerRadius
        signUpButton.layer.masksToBounds = true
        signUpButton.layer.borderWidth = borderWith
        signUpButton.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
}
//Colors
let yellow = UIColor.init(red: 242/255, green: 178/255, blue: 60/255, alpha: 1)
let lightGreen = UIColor.init(red: 135/255, green: 191/255, blue: 170/255, alpha: 1)
let darkerGreen = UIColor.init(red: 87/255, green: 165/255, blue: 115/255, alpha: 1)
let brown = UIColor.init(red: 89/255, green: 74/255, blue: 1/255, alpha: 1)
let red = UIColor.init(red: 216/255, green: 71/255, blue: 58/255, alpha: 1)

//UI constants
let cornerRadius: CGFloat = 5
let borderWith: CGFloat = 1
