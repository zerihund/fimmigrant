//
//  Questionare2ViewController.swift
//  fimmigrant
//
//  Created by Kristófer Ívar Knutsen on 29/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class Questionare2ViewController: UITableViewController {

    // Variables
    var arrayFromQ1: [Bool] = []
    // Array that will contain the names of all the tasks that have already been done by the user
    var taskAlreadyComplete = [String]()
    // Get the app delegate objects
    var app : AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    // Outlets
    @IBOutlet var theTableView: UITableView!
    @IBOutlet weak var kelaSwitch: UISwitch!
    @IBOutlet weak var hslSwitch: UISwitch!
    @IBOutlet weak var housingSwitch: UISwitch!
    @IBOutlet weak var taxSwitch: UISwitch!
    @IBOutlet weak var safetySwitch: UISwitch!
    @IBOutlet weak var hygineSwitch: UISwitch!
    @IBOutlet weak var hotWorkSwitch: UISwitch!
    @IBOutlet weak var finnishIDSwitch: UISwitch!
    @IBOutlet weak var finishButton: UIButton!
    // Table View Cell
    @IBOutlet weak var c1: UITableViewCell!
    @IBOutlet weak var c2: UITableViewCell!
    @IBOutlet weak var c3: UITableViewCell!
    @IBOutlet weak var c4: UITableViewCell!
    @IBOutlet weak var c5: UITableViewCell!
    @IBOutlet weak var c6: UITableViewCell!
    @IBOutlet weak var c7: UITableViewCell!
    @IBOutlet weak var c8: UITableViewCell!
    @IBOutlet weak var c9: UITableViewCell!
    
    // Boolians
    var kelaCardSwitchIsOn = false
    var hslCardSwitchIsOn = false
    var generalHousingSwitchIsOn = false
    var taxCardSwitchIsOn = false
    var safetyCardSwitchIsOn = false
    var hygienePassportSwitchIsOn = false
    var hotWorkLicenseSwitchIsOn = false
    var finnishIdentityCardSwitchIsOn = false
    
    
    // Array to hold the values that user wanna add
    var taskWantToAdd = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        setupUI()
        setSwitchStates()
        print(arrayFromQ1)
    }

    // Set the state of the switches accordingly if they were selected in the previous view or not
    func setSwitchStates () {
        self.taskAlreadyComplete.removeAll()
        self.taskWantToAdd.removeAll()
        
        // for kela card task
        if (arrayFromQ1[0] == true) {
            self.taskAlreadyComplete.append("Kela card")
            kelaSwitch.isEnabled = false
        } else {
            kelaSwitch.isEnabled = true
        }
        
        // for hsl card task
        if (arrayFromQ1[1] == true) {
            self.taskAlreadyComplete.append("HSL Card")
            hslSwitch.isEnabled = false
        } else {
            hslSwitch.isEnabled = true
        }
        
        // for general housing allowance
        if (arrayFromQ1[2] == true) {
            self.taskAlreadyComplete.append("General Housing Allowance")
            housingSwitch.isEnabled = false
        } else {
            housingSwitch.isEnabled = true
        }
        
        // for tax card
        if (arrayFromQ1[3] == true) {
            self.taskAlreadyComplete.append("Tax Card")
            taxSwitch.isEnabled = false
        } else {
            taxSwitch.isEnabled = true
        }
        
        // for occupational safety card
        if (arrayFromQ1[4] == true) {
            self.taskAlreadyComplete.append("Occupational Safety Card")
            safetySwitch.isEnabled = false
        } else {
            safetySwitch.isEnabled = true
        }
        
        // for hygiene passport
        if (arrayFromQ1[5] == true) {
            self.taskAlreadyComplete.append("Hygiene Passport")
            hygineSwitch.isEnabled = false
        } else {
            hygineSwitch.isEnabled = true
        }
        
        // for hotwork license
        if (arrayFromQ1[6] == true) {
            self.taskAlreadyComplete.append("Hot work license")
            hotWorkSwitch.isEnabled = false
        } else {
            hotWorkSwitch.isEnabled = true
        }
        
        // for identity card
        if (arrayFromQ1[7] == true) {
            self.taskAlreadyComplete.append("Finnish identity card")
            finnishIDSwitch.isEnabled = false
        } else {
            finnishIDSwitch.isEnabled = true
        }
    }
    
    // Set up the UI colors and stuffs
    func setupUI() {
        
        // Finish button style
        finishButton.backgroundColor = UIColor.init(named: "shakeBlue")
        finishButton.tintColor = .white
        finishButton.layer.cornerRadius = 12
        finishButton.layer.borderWidth = 2
        finishButton.layer.borderColor = #colorLiteral(red: 0.1294117647, green: 0.3098039216, blue: 0.7607843137, alpha: 1)
        
        // Set background image
        let bgImage = UIImageView()
        bgImage.image = UIImage.init(named: "statue")
        bgImage.alpha = 0.2
        bgImage.contentMode = .scaleAspectFill
        
        // Style the table view
        theTableView.backgroundView = bgImage
        theTableView.backgroundColor = .white
        
        // Cell style
        c1.backgroundColor = .none
        c2.backgroundColor = .none
        c3.backgroundColor = .none
        c4.backgroundColor = .none
        c5.backgroundColor = .none
        c6.backgroundColor = .none
        c7.backgroundColor = .none
        c8.backgroundColor = .none
        c9.backgroundColor = .none
        let alphaValue: CGFloat = 0.5
        c1.alpha = alphaValue
        c2.alpha = alphaValue
        c3.alpha = alphaValue
        c4.alpha = alphaValue
        c5.alpha = alphaValue
        c6.alpha = alphaValue
        c7.alpha = alphaValue
        c8.alpha = alphaValue
        c9.alpha = alphaValue
        
    }
    
    // If the view is reloaded, if you go back to questionare 1 and come back to this view
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        print("view did load again")
    }
    
    // When the Finished button is clicked, find the states of the switches
    @IBAction func buttonClick(_ sender: Any) {
        // check for task already complete
        for each in self.taskAlreadyComplete {
            switch each {
            case "Kela card":
                kelaCardSwitchIsOn = true
            case "HSL Card":
                hslCardSwitchIsOn = true
            case "General Housing Allowance":
                generalHousingSwitchIsOn = true
            case "Tax Card":
                taxCardSwitchIsOn = true
            case "Occupational Safety Card":
                safetyCardSwitchIsOn = true
            case "Hygiene Passport":
                hygienePassportSwitchIsOn = true
            case "Hot work license":
                hotWorkLicenseSwitchIsOn = true
            case "Finnish identity card":
                finnishIdentityCardSwitchIsOn = true
            default:
                print("default")
            }
        }
        
        // Check for task user want to do
        if kelaCardSwitchIsOn {
            self.taskWantToAdd.append("Kela card")
        }
        if hslCardSwitchIsOn {
            self.taskWantToAdd.append("HSL Card")
        }
        if generalHousingSwitchIsOn {
            self.taskWantToAdd.append("General Housing Allowance")
        }
        if safetyCardSwitchIsOn {
            self.taskWantToAdd.append("Occupational Safety Card")
        }
        if hygienePassportSwitchIsOn {
            self.taskWantToAdd.append("Hygiene Passport")
        }
        if hotWorkLicenseSwitchIsOn {
            self.taskWantToAdd.append("Hot work license")
        }
        if taxCardSwitchIsOn {
            self.taskWantToAdd.append("Tax Card")
        }
        if finnishIdentityCardSwitchIsOn {
            self.taskWantToAdd.append("Finnish identity card")
        }
        
        // Add task to users
        if let user = app.user {
            let taskList = TaskMaster.getTaskList()
            for each in taskList {
                for item in self.taskWantToAdd {
                    if each.title == item {
                        user.addTask(task: each)
                    }
                }
            }
            
            // Check if user already finish the task
            for each in user.taskList {
                for item in self.taskAlreadyComplete {
                    if each.title == item {
                        each.setDone()
                    }
                }
            }
        }
        // after all the check and added the tasks to the user list and set their properties properly, move
        // the user to the tabView
        performSegue(withIdentifier: "goToTabView", sender: self)
    }
    
    // Functions for setting the stage and bools from UISwitch in the UI
    @IBAction func kelaSwitchPress(_ sender: UISwitch) {
        if (sender.isOn == true){
            kelaCardSwitchIsOn = true
            print(kelaCardSwitchIsOn)
        }
        else {
            kelaCardSwitchIsOn = false
            print(kelaCardSwitchIsOn)
        }
    }
    @IBAction func hslSwitchPress(_ sender: UISwitch) {
        if (sender.isOn == true){
            hslCardSwitchIsOn = true
            print(hslCardSwitchIsOn)
        }
        else {
            hslCardSwitchIsOn = false
            print(hslCardSwitchIsOn)
        }
    }
    @IBAction func houseSwitchPress(_ sender: UISwitch) {
        if (sender.isOn == true){
            generalHousingSwitchIsOn = true
            print(generalHousingSwitchIsOn)
        }
        else {
            generalHousingSwitchIsOn = false
            print(generalHousingSwitchIsOn)
        }
    }
    @IBAction func taxSwitchPress(_ sender: UISwitch) {
        if (sender.isOn == true){
            taxCardSwitchIsOn = true
            print(taxCardSwitchIsOn)
        }
        else {
            taxCardSwitchIsOn = false
            print(taxCardSwitchIsOn)
        }
    }
    @IBAction func safetySwitchPress(_ sender: UISwitch) {
        if (sender.isOn == true){
            safetyCardSwitchIsOn = true
            print(safetyCardSwitchIsOn)
        }
        else {
            safetyCardSwitchIsOn = false
            print(safetyCardSwitchIsOn)
        }
    }
    @IBAction func hygieneSwitchPress(_ sender: UISwitch) {
        if (sender.isOn == true){
            hygienePassportSwitchIsOn = true
            print(hygienePassportSwitchIsOn)
        }
        else {
            hygienePassportSwitchIsOn = false
            print(hygienePassportSwitchIsOn)
        }
    }
    @IBAction func hotSwitchPress(_ sender: UISwitch) {
        if (sender.isOn == true){
            hotWorkLicenseSwitchIsOn = true
            print(hotWorkLicenseSwitchIsOn)
        }
        else {
            hotWorkLicenseSwitchIsOn = false
            print(hotWorkLicenseSwitchIsOn)
        }
    }
    @IBAction func identityPress(_ sender: UISwitch) {
        if (sender.isOn == true){
            finnishIdentityCardSwitchIsOn = true
            print(finnishIdentityCardSwitchIsOn)
        }
        else {
            finnishIdentityCardSwitchIsOn = false
            print(finnishIdentityCardSwitchIsOn)
        }
    }
}
