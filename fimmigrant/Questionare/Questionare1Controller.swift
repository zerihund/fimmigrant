//
//  Questionare1Controller.swift
//  fimmigrant
//
//  Created by Kristófer Ívar Knutsen on 29/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class Questionare1Controller: UITableViewController {

    // Outlets
    @IBOutlet var theTableView: UITableView!
    @IBOutlet weak var kelaCardLabel: UILabel!
    @IBOutlet weak var hslCardLabel: UILabel!
    @IBOutlet weak var generalHousingLabel: UILabel!
    @IBOutlet weak var taxCardLabel: UILabel!
    @IBOutlet weak var safetyCardLabel: UILabel!
    @IBOutlet weak var hygienePassportLabel: UILabel!
    @IBOutlet weak var hotWorkLicenseLabel: UILabel!
    @IBOutlet weak var finnishIdentityCardLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var c1: UITableViewCell!
    @IBOutlet weak var c2: UITableViewCell!
    @IBOutlet weak var c3: UITableViewCell!
    @IBOutlet weak var c4: UITableViewCell!
    @IBOutlet weak var c5: UITableViewCell!
    @IBOutlet weak var c6: UITableViewCell!
    @IBOutlet weak var c7: UITableViewCell!
    @IBOutlet weak var c8: UITableViewCell!
    @IBOutlet weak var c9: UITableViewCell!
    
    
    // Variables
    var kelaCardSwitchIsOn = false
    var hslCardSwitchIsOn = false
    var generalHousingSwitchIsOn = false
    var taxCardSwitchIsOn = false
    var safetyCardSwitchIsOn = false
    var hygienePassportSwitchIsOn = false
    var hotWorkLicenseSwitchIsOn = false
    var finnishIdentityCardSwitchIsOn = false
    
    // Arrays
    var boolArray: [Bool] = []
    
    // Switches functions
    @IBAction func kelaSwitch(_ sender: UISwitch) {
        if (sender.isOn == true){
            kelaCardSwitchIsOn = true
            print(kelaCardSwitchIsOn)
        }
        else {
            kelaCardSwitchIsOn = false
            print(kelaCardSwitchIsOn)
        }
    }
    
    @IBAction func hslCardSwitch(_ sender: UISwitch) {
        if (sender.isOn == true){
            hslCardSwitchIsOn = true
            print(hslCardSwitchIsOn)
        }
        else {
            hslCardSwitchIsOn = false
            print(hslCardSwitchIsOn)
        }
    }
    
    @IBAction func generalHousingSwitch(_ sender: UISwitch) {
        if (sender.isOn == true){
            generalHousingSwitchIsOn = true
            print(generalHousingSwitchIsOn)
        }
        else {
            generalHousingSwitchIsOn = false
            print(generalHousingSwitchIsOn)
        }
    }
    
    @IBAction func taxCardSwitch(_ sender: UISwitch) {
        if (sender.isOn == true){
            taxCardSwitchIsOn = true
            print(taxCardSwitchIsOn)
        }
        else {
            taxCardSwitchIsOn = false
            print(taxCardSwitchIsOn)
        }
    }
    
    @IBAction func safetyCardSwitch(_ sender: UISwitch) {
        if (sender.isOn == true){
            safetyCardSwitchIsOn = true
            print(safetyCardSwitchIsOn)
        }
        else {
            safetyCardSwitchIsOn = false
            print(safetyCardSwitchIsOn)
        }
    }
    
    @IBAction func hygienePassportSwitch(_ sender: UISwitch) {
        if (sender.isOn == true){
            hygienePassportSwitchIsOn = true
            print(hygienePassportSwitchIsOn)
        }
        else {
            hygienePassportSwitchIsOn = false
            print(hygienePassportSwitchIsOn)
        }
    }
    
    @IBAction func hotWorkLicenseSwitch(_ sender: UISwitch) {
        if (sender.isOn == true){
            hotWorkLicenseSwitchIsOn = true
            print(hotWorkLicenseSwitchIsOn)
        }
        else {
            hotWorkLicenseSwitchIsOn = false
            print(hotWorkLicenseSwitchIsOn)
        }
    }
    
    @IBAction func finnishIdentityCardSwitch(_ sender: UISwitch) {
        if (sender.isOn == true){
            finnishIdentityCardSwitchIsOn = true
            print(finnishIdentityCardSwitchIsOn)
        }
        else {
            finnishIdentityCardSwitchIsOn = false
            print(finnishIdentityCardSwitchIsOn)
        }
    }
    
    // First load view
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        setupUI()
    }
    
    // If the view is loaded again, if you go to questionare 2 and go back to this one
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        boolArray.removeAll()
        print("view did load again")
        print(boolArray)
    }

    // Setup the UI
    func setupUI() {
        // Continue button style
        continueButton.backgroundColor = UIColor.init(named: "shakeBlue")
        continueButton.tintColor = .white
        continueButton.layer.cornerRadius = 12
        continueButton.layer.borderWidth = 2
        continueButton.layer.borderColor = #colorLiteral(red: 0.1294117647, green: 0.3098039216, blue: 0.7607843137, alpha: 1)
        
        // Background image
        let bgImage = UIImageView()
        bgImage.image = UIImage.init(named: "tree")
        bgImage.alpha = 0.2
        bgImage.contentMode = .scaleAspectFill
        
        // TableViewUI
        theTableView.backgroundView = bgImage
        theTableView.backgroundColor = .white
        
        // Cell style
        c1.backgroundColor = .none
        c2.backgroundColor = .none
        c3.backgroundColor = .none
        c4.backgroundColor = .none
        c5.backgroundColor = .none
        c6.backgroundColor = .none
        c7.backgroundColor = .none
        c8.backgroundColor = .none
        c9.backgroundColor = .none
        let alphaValue: CGFloat = 0.5
        c1.alpha = alphaValue
        c2.alpha = alphaValue
        c3.alpha = alphaValue
        c4.alpha = alphaValue
        c5.alpha = alphaValue
        c6.alpha = alphaValue
        c7.alpha = alphaValue
        c8.alpha = alphaValue
        c9.alpha = alphaValue
        
    }

    // The continue button. Adds bools to array and does the segue
    @IBAction func continueButton(_ sender: UIButton) {
        // Add the bool values of the switches to an array to be segue-d to the next view
        boolArray.append(kelaCardSwitchIsOn)
        boolArray.append(hslCardSwitchIsOn)
        boolArray.append(generalHousingSwitchIsOn)
        boolArray.append(taxCardSwitchIsOn)
        boolArray.append(safetyCardSwitchIsOn)
        boolArray.append(hygienePassportSwitchIsOn)
        boolArray.append(hotWorkLicenseSwitchIsOn)
        boolArray.append(finnishIdentityCardSwitchIsOn)
        print(boolArray)
        performSegue(withIdentifier: "questionSegue", sender: self)
    }
    
    // The segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! Questionare2ViewController
        vc.arrayFromQ1 = boolArray
    }
}
