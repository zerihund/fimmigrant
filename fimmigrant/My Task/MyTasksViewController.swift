//
//  MyTasksViewController.swift
//  fimmigrant
//
//  Created by iosdev on 27/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class MyTasksViewController: UIViewController {
    
    var sectionTitles: [String] = [NSLocalizedString("Ongoing Tasks", comment: ""), NSLocalizedString("Finished Tasks", comment: "")]
    var taskArr : [Task] = []
    var emptyArr: [Task] = []
    var tableViewArray: [[Task]] = [[],[]]
    var app : AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        taskArr = (app.user?.taskList ?? emptyArr)
        
        //tableviewArr is where the table view gets its data.
        tableViewArray[0].removeAll()
        tableViewArray[1].removeAll()
        for each in taskArr {
            if (each.taskComplete == true){
                tableViewArray[1].append(each)
            } else {
                tableViewArray[0].append(each)
            }
        }
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addToTable()
    }
    
    func addToTable() {
        taskArr.removeAll()
        taskArr = ((app.user?.taskList ?? emptyArr))
        tableViewArray[0].removeAll()
        tableViewArray[1].removeAll()
        for each in taskArr {
            if (each.taskComplete == true){
                tableViewArray[1].append(each)
            } else {
                tableViewArray[0].append(each)
            }
        }
        tableView.reloadData()
    }

}

extension MyTasksViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewArray[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let task = tableViewArray[indexPath.section][indexPath.row]
        let cellIdentifier = "TaskCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! TaskTableViewCell
        
        cell.setTaskForCell(theTask: task)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return sectionTitles[section] + ": " + String(tableViewArray[0].count) + " " + NSLocalizedString("tasks", comment: "")
        } else {
            return sectionTitles[section] + ": " + String(tableViewArray[1].count) + " " + NSLocalizedString("tasks", comment: "")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "taskDetail"{
            if let snder = sender as? TaskTableViewCell {
                if let destVC = segue.destination as? TaskDetailViewController {
                    print("u sure info arrives")
                    destVC.task = snder.task
                }
            }
            
        }
    }
    
    @IBAction func didUnwindFromBMIView(_ sender: UIStoryboardSegue) {
        guard let _ = sender.source as? TaskDetailViewController else { return }
    }
    
}
