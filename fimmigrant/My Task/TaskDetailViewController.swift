//
//  TaskDetailViewController.swift
//  fimmigrant
//
//  Created by iosdev on 01/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class TaskDetailViewController: UIViewController {
    var task: Task? = nil
    
    @IBOutlet weak var finishTaskButton: UIButton!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var LabelDesc: UILabel!
    @IBOutlet weak var LabelPreq: UILabel!
    @IBOutlet weak var LabelSteps: UILabel!
    @IBOutlet weak var LabelLink: UILabel!
    
    //Display Task to the mytask detail
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let _task = task {
            if _task.taskComplete == true {
                self.finishTaskButton.isHidden = true
            }
            self.finishTaskButton.layer.cornerRadius = 5
            taskLabel.text = _task.title
            LabelDesc.text = _task.desc
            LabelPreq.text = _task.prerequisite
            var instru = "*"
            for step in _task.stepArray {
                instru += " \(step.instruction) *"
                print("what is it")
                print(instru)
                LabelSteps.text = "\(instru)"
            }
            LabelLink.text = _task.link
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // make a link label clickable
        let linkTap = UITapGestureRecognizer(target: self, action: #selector(TaskDetailViewController.tapFunction))
        LabelLink.isUserInteractionEnabled = true
        LabelLink.addGestureRecognizer(linkTap)
    }
    //tab fuction to make link clickable
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        print("tap working")
        let webVc = UIStoryboard.init(name: "TasksDetail", bundle: nil).instantiateViewController(withIdentifier: "web") as! WebViewController
        print(task?.link ?? "link not found")
        webVc.url = task?.link
        self.navigationController?.pushViewController(webVc, animated: true)
    }
    // task done button 
    @IBAction func taskDone(_ sender: UIButton) {
        if let currentTask = self.task {
            if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                if let user = myDelegate.user {
                    let _ = user.gainExperience(exp: 300)
                    AchivManager.checkAchivCon(user: user, checkAchiv: "")
                }
            }
            currentTask.setDone()
        }
    }
    
}
