//
//  WebViewController.swift
//  fimmigrant
//
//  Created by iosdev on 01/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {

    @IBOutlet weak var webview: UIWebView!
    var url:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        // loading webview 
        webview.loadRequest(URLRequest(url:URL(string: url!)!))
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
