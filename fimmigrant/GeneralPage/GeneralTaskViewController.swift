//
//  GeneralTaskViewController.swift
//  fimmigrant
//
//  Created by iosdev on 06/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class GeneralTaskViewController: UIViewController {
    var task:Task?
    
    @IBOutlet weak var preqLabel: UILabel!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    var app : AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let linkTap = UITapGestureRecognizer(target: self, action: #selector(GeneralTaskViewController.tapFunction))
        linkLabel.isUserInteractionEnabled = true
        linkLabel.addGestureRecognizer(linkTap)

        // Do any additional setup after loading the view.
    }
    //functuion for clickable link label 
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        print("tap working")
        let webVc = UIStoryboard.init(name: "Tasks", bundle: nil).instantiateViewController(withIdentifier: "allweb") as! AllWebViewController
        print(task?.link ?? "link not found")
        webVc.url = task?.link
        self.navigationController?.pushViewController(webVc, animated: true)
    }
    //add task to user task list
    @IBAction func addTaskButtonTap(_ sender: UIButton) {
        if let thisTask = task {
            app.user?.addTask(task: thisTask)
            performSegue(withIdentifier: "unwindToAllTasks", sender: self)
        }
    }
    
    @IBAction func didUnwindFromBMIView(_ sender: UIStoryboardSegue) {
        guard let _ = sender.source as? GeneralTaskViewController else { return }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let _task = task {
            
            titleLabel.text = _task.title
            descLabel.text = _task.desc
            preqLabel.text = _task.prerequisite
            linkLabel.text = _task.link
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
