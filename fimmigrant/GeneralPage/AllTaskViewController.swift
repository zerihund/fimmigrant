//
//  AllTaskViewController.swift
//  fimmigrant
//
//  Created by iosdev on 03/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class AllTaskViewController: UIViewController {
    //MARK: Outlets
    @IBOutlet weak var allTaskTableView: UITableView!
    
    //MARK: Variables
    var preAllTaskArr: [Task] = []
    var sectionTitles: [String] = [NSLocalizedString("Featured Tasks", comment: ""), NSLocalizedString("General Tasks", comment: "")]
    var taskArr : [Task] = []
    var tableViewArr : [[Task]] = [[/* ongoing*/],[/*done*/]]
    var app : AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        
        //self.taskArr = createArray()
        self.allTaskTableView.delegate = self
        self.allTaskTableView.dataSource = self
        
        //self.allTaskTableView.separatorColor = UIColor.black
        
        self.initPopulateSections()
        self.addToFeatured()
    }
    
    func curateAllTaskList() {
        var preAllTaskArray = [Task]()
        let allTask = TaskMaster.getTaskList()
        if let userTask = app.user?.taskList {
            for each in allTask {
                var userAlreadyHasTask = false
                for item in userTask {
                    if each.title == item.title {
                        userAlreadyHasTask = true
                    }
                }
                if userAlreadyHasTask == false {
                    preAllTaskArray.append(each)
                }
            }
        }
        
        for item in preAllTaskArray {
            // Add Item to allTaskArr if it is not in MyTasks
            if (app.user?.taskList)?.firstIndex(of: item) == nil {
                taskArr.append(item)
                print("\(item.title) is added to AllTaskArr in curateAllTaskList()")
            }
        }
    }
    
    func initPopulateSections () {// populates sections featured and general
        self.curateAllTaskList()
        
        //empty the array and repopulate. Why? because otherwise finding the section to remove from is a pain.
        tableViewArr[0].removeAll()
        tableViewArr[1].removeAll()
        
        //populate the array based on the state of the task
        for item in taskArr{
            tableViewArr[1].append(item)
        }
    }
    
    func addToFeatured() {
        //run a for loop twice and add the randomly selected task to the featured if that index exists
        for _ in 1...2{
            if tableViewArr[1].count > 2 {
                //remove the randomly selected task and put it in featured
                let random1: Int = randomNumbers()
                let randomTask1 = tableViewArr[1].remove(at: random1)
                tableViewArr[0].append(randomTask1)
            }
        }
    }
    
    func randomNumbers() -> Int {
        //Find how many tasks alltogether there are then choose to randomly so we can add them to featured tasks
        let max = tableViewArr[1].count
        
        let number = Int.random(in: 0..<max)
        
        return number
    }

}

extension AllTaskViewController: AllTaskCellDelegate {
    
    
    func addTask(thisTask: Task) {
        let position = taskArr.firstIndex(of: thisTask) ?? 0
        
        //remove the task from the taskArr and the tableViewArr.
        //The if else checks which subArray it is in and handles removal.
        
        //remove from taskArr
        let removedTask = taskArr.remove(at: position)
        print(removedTask.title)
        //remove from tableview's Array as well
        if ((tableViewArr[0].firstIndex(of: thisTask)) != nil) { //If its array 0
            let num0 = tableViewArr[0].firstIndex(of: thisTask)
            tableViewArr[0].remove(at: num0!)
        }else if (tableViewArr[1].firstIndex(of: thisTask)) != nil {//If its array 1
            let num1 = tableViewArr[1].firstIndex(of: thisTask)
            tableViewArr[1].remove(at: num1!)
        }
        
        // adds to user tasks as well as the tableViewArr of MyTaskVC. It will show up becaus of viewWillAppear
        app.user?.addTask(task: removedTask)
        // reference myTaskVC and send it the task
//        let myTaskVC = MyTasksViewController(nibName: "MyTasksViewController", bundle: nil)
//        myTaskVC.tableViewArray.append(removedTask)
//
        
        self.allTaskTableView.reloadData()
    }
}



extension AllTaskViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewArr[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let task = tableViewArr[indexPath.section][indexPath.row]
        let cellIdentifier = "AllTaskCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! AllTaskTableViewCell
        
        cell.delegate = self
        //cell.layer.borderColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
       // cell.layer.borderWidth = 4
        cell.setTaskForCell(theTask: task)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    //send data to task detail
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sour = sender as? AllTaskTableViewCell{
            if let dest = segue.destination as? GeneralTaskViewController{
                dest.task = sour.taskItem
            }
        }
    }
}
