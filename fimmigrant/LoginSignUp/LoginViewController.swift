//
//  LoginViewController.swift
//  fimmigrant
//
//  Created by iosdev on 24/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate{
   
    // Constatns
    let radius: CGFloat = 12
    let backgroundImage = UIImageView()
    let logoImageView = UIImageView()
    let appNameLabelView = UITextView()
    
    // Outlets
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    // Variables
    private (set) var name = ""
    private (set) var pass = ""
    var loginStackView = UIStackView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        loginStackView = UIStackView(arrangedSubviews: [username, password, loginButton, registerButton])
        setupLayout()
        setupBackground()
        self.password.isSecureTextEntry = true
        self.username.delegate = self
        self.password.delegate = self
        UserManager.autoLogin(view: self)
        
        // Obervers for the keyboard on/off
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // Stop listening to the keyboard observers
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // Check the status of the keyboard. Move the view up and down accordingly
    @objc func keyboardWillChange (notification: Notification) {
        guard let keyboardRectangle = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        if (notification.name == UIResponder.keyboardWillShowNotification || notification.name == UIResponder.keyboardWillChangeFrameNotification) {
            view.frame.origin.y = -keyboardRectangle.height
        } else {
             view.frame.origin.y = 0
        }
    }
    
    // Functions
    func setupLayout() {
        // Container setup for layouts
        let topContainer = UIView()
        view.addSubview(topContainer)
        view.addSubview(appNameLabelView)
        
        // Contrainer constraints
        topContainer.translatesAutoresizingMaskIntoConstraints = false
        topContainer.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        topContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        topContainer.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        
        // Logo view setup
        logoImageView.image = UIImage.init(named: "logo")
        view.addSubview(logoImageView)
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImageView.centerYAnchor.constraint(equalToSystemSpacingBelow: topContainer.centerYAnchor, multiplier: 0.5).isActive = true
        logoImageView.heightAnchor.constraint(equalTo: topContainer.heightAnchor, multiplier: 0.5).isActive = true
        logoImageView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 60).isActive = true
        logoImageView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -60).isActive = true
        logoImageView.contentMode = .scaleAspectFit
        
        // Button styling
        loginButton.backgroundColor = UIColor.init(named: "shakeBlue")
        registerButton.backgroundColor = UIColor.init(named: "shakeBlue")
        username.alpha = 0.8
        password.alpha = 0.8
        registerButton.layer.cornerRadius = radius
        loginButton.layer.cornerRadius = radius
        username.layer.cornerRadius = radius
        password.layer.cornerRadius = radius
        username.clipsToBounds = true
        password.clipsToBounds = true
        loginButton.tintColor = UIColor.white
        registerButton.tintColor = UIColor.white
        
        // Stack view styling
        view.addSubview(loginStackView)
        loginStackView.translatesAutoresizingMaskIntoConstraints = false
        loginStackView.axis = .vertical
        loginStackView.spacing = 8
        loginStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        loginStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginStackView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor, multiplier: 0.7).isActive = true
    }
    
    // Setup the background picture
    private func setupBackground() {
        view.addSubview(backgroundImage)
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        backgroundImage.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImage.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundImage.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundImage.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        backgroundImage.image = UIImage.init(named: "Helsinki")
        backgroundImage.contentMode = .scaleAspectFill
        backgroundImage.alpha = 0.35
        view.sendSubviewToBack(backgroundImage)
    }
    
    // Check the textfield
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        checkCredentials()
        return true
    }
    
    // Login button
    @IBAction func loginRequest(_ sender: UIButton) {
        checkCredentials()
        loginCheck()
    }
    
    // Check the login and password
    func loginCheck()  {
        guard let url = URL(string:"http://media.mw.metropolia.fi/wbma/login") else {
            fatalError("fail to create url")
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: ["username": name, "password": pass], options: .prettyPrinted)
        } catch let error {
            print("error is here")
            print(error.localizedDescription)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // set up the connection task
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            if let error = error {
                print("Client error \(error)")
            }
            
            if let data = data, let _ = String(data: data, encoding: .utf8) {
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                print(json!)
                if let token = json?["token"] as? String {
                    CoreDataHandler.checkData(token: token, logInView: self, signUpView: nil, profileView: nil)
                } else {
                    return
                }
            }
        }
        
        task.resume()
    }
    
    // Check the credentials if the pass the
    func checkCredentials() {
        if (self.username.text == "" || self.password.text == "") {
            self.loginButton.isEnabled = false
        } else {
            self.loginButton.isEnabled = true
            name = self.username.text ?? ""
            pass = self.password.text ?? ""
        }
    }
    
    // Auto login function
    func autoLogin () {
        performSegue(withIdentifier: "loginSegue", sender: self)
    }
    
    // Logiin function
    func logIn() {
        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            myDelegate.initLogIn()
            if let user = myDelegate.user {
                AchivManager.checkAchivCon(user: user, checkAchiv: "back")
            }
        }
        self.performSegue(withIdentifier: "loginSegue", sender: self)
    }
}
