//
//  SignUpController.swift
//  fimmigrant
//
//  Created by iosdev on 24/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class SignUpController: UIViewController {
    
    // Outlets
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    
    // Variables
    var nameIsValid = false
    var passwordIsValid = false
    var stackView = UIStackView()
    
    // Constatns
    let usernameIcon = UIImageView()
    let passwordIcon = UIImageView()
    let radius: CGFloat = 12
    let backgroundImageView = UIImageView()
    let bottomContainer = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        password.isSecureTextEntry = true
        containerSetup()
        buttonSetup()
        stackViewSetup()
        backgroundImageSetup()

        // Obervers for the keyboard on/off
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // Deregister keyboard observer
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // UI setups
    func containerSetup () {
        view.addSubview(bottomContainer)
        bottomContainer.translatesAutoresizingMaskIntoConstraints = false
        bottomContainer.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        bottomContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        bottomContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        bottomContainer.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
    }
    
    func buttonSetup() {
        name.placeholder = "Username"
        name.alpha = 0.8
        name.layer.cornerRadius = radius
        name.clipsToBounds = true
        name.heightAnchor.constraint(equalToConstant: 40).isActive = true
        stackView = UIStackView(arrangedSubviews: [name, password, signUpButton])
        password.placeholder = "Password"
        password.alpha = 0.8
        password.layer.cornerRadius = radius
        password.clipsToBounds = true
        password.heightAnchor.constraint(equalToConstant: 40).isActive = true
        signUpButton.layer.cornerRadius = radius
        signUpButton.tintColor = UIColor.white
        signUpButton.isEnabled = false
        signUpButton.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        signUpButton.alpha = 1
        signUpButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        usernameIcon.image = UIImage.init(named: "user")
        view.addSubview(usernameIcon)
        passwordIcon.image = UIImage.init(named: "password")
        view.addSubview(passwordIcon)
        usernameIcon.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        usernameIcon.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    func stackViewSetup () {
        view.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 8
        stackView.bottomAnchor.constraint(equalTo: bottomContainer.bottomAnchor, constant: -5).isActive = true
        stackView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor, multiplier: 0.7).isActive = true
        stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    func backgroundImageSetup () {
        view.addSubview(backgroundImageView)
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        backgroundImageView.image = UIImage.init(named: "house")
        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.alpha = 0.35
        view.sendSubviewToBack(backgroundImageView)
    }
    
    // Check the status of the keyboard. Move the view up and down accordingly
    @objc func keyboardWillChange (notification: Notification) {
        guard let keyboardRectangle = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        if (notification.name == UIResponder.keyboardWillShowNotification || notification.name == UIResponder.keyboardWillChangeFrameNotification) {
            view.frame.origin.y = -keyboardRectangle.height
            
        } else {
            view.frame.origin.y = 0
        }
    }
    
    // function gets called when user is editing username text, this check for validity of username
    // user name must be longer than 5 characters and does not resemble any other users' names
    @IBAction func nameInputChanged(_ sender: UITextField) {
        if let username = self.name.text {
            if (username.count >= 3) {
                print(username)
                guard let url = URL(string: "http://media.mw.metropolia.fi/wbma/users/username/" + username) else {
                    fatalError("url fails to init")
                }
                // set up http request method
                let request = URLRequest(url: url)
                
                // set up the connection task
                let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                    if let error = error {
                        print("Client error \(error)")
                    }
                    
                    if let data = data, let _ = String(data: data, encoding: .utf8) {
                        DispatchQueue.main.async {
                            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                            let ans = json?["available"] as! Bool
                            //print(ans)
                            if ans {
                                self.nameIsValid = true
                                self.signUpButton.backgroundColor = .gray
                                self.signUpButton.setTitle(NSLocalizedString("Register", comment: ""), for: .normal)
                                self.signUpButton.alpha = 1
                            } else {
                                self.nameIsValid = false
                                self.signUpButton.backgroundColor = .red
                                self.signUpButton.tintColor = .black
                                self.signUpButton.layer.borderWidth = 1
                                self.signUpButton.setTitle(NSLocalizedString("Username taken", comment: ""), for: .normal)
                                self.signUpButton.alpha = 1
                            }
                            self.checkValid()
                        }
                    }
                }
                task.resume()
            } else {
                self.nameIsValid = false
                self.checkValid()
            }
        } else {
            self.nameIsValid = false
            self.checkValid()
        }
    }
    
    // function gets called when user is editing password text, this check for validity of password
    // password must be longer than 5 characters
    @IBAction func passwordInputChanged(_ sender: UITextField) {
        if let psword = password.text {
            if psword.count < 5 {
                self.passwordIsValid = false
                self.signUpButton.backgroundColor = .red
                self.signUpButton.tintColor = .black
                self.signUpButton.layer.borderWidth = 1
                self.signUpButton.setTitle(NSLocalizedString("Enter valid password", comment: ""), for: .normal)
            } else {
                self.passwordIsValid = true
                self.signUpButton.setTitle(NSLocalizedString("Register", comment: ""), for: .normal)
            }
            self.checkValid()
        }
    }
    
    // check if both user name and password is valid
    func checkValid() {
        if (nameIsValid && passwordIsValid) {
            setButtonState(state: true)
        } else {
            setButtonState(state: false)
        }
    }
    //  change register button state accordingly
    func setButtonState(state: Bool) {
        if state {
            self.signUpButton.isEnabled = true
            signUpButton.alpha = 1
            signUpButton.backgroundColor = UIColor.init(named: "lightGreen")

        } else {
            self.signUpButton.isEnabled = false
            //signUpButton.alpha = 0.3
            //signUpButton.backgroundColor = UIColor.init(named: "red")

        }
    }
    
    // sign user up when click sign up button
    @IBAction func signUserUp(_ sender: UIButton) {
        guard let usrName = self.name.text else {
            return
        }
        guard let pswd = self.password.text else {
            return
        }
        guard let url = URL(string: "http://media.mw.metropolia.fi/wbma/users") else {
            fatalError("url fails to init")
        }
        // set up http request method
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: ["username": usrName, "password": pswd, "email": randomEmail()], options: .prettyPrinted)
        } catch let error {
            print("error is here")
            print(error.localizedDescription)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // set up the connection task
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            if let error = error {
                print("Client error \(error)")
            }
            
            if let data = data, let _ = String(data: data, encoding: .utf8) {
                DispatchQueue.main.async {
                    let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    print(json!)
                    CoreDataHandler.firstTimeSignUpInit(name: usrName, password: pswd, view: self)
                }
            }
        }
        
        task.resume()
    }
    
    // log in
    func logIn() {
        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            myDelegate.initLogIn()
            if let user = myDelegate.user {
                AchivManager.checkAchivCon(user: user, checkAchiv: "new")
            }
        }
        self.performSegue(withIdentifier: "awayFromSignUpSegue", sender: self)
    }
    
    // generate random email
    func randomEmail() -> String{
        var email = ""
        var i = 0
        while (i < 60) {
            let alphabet = "abcdefghijklmnopqrstuvWYZABCDEFGHIJKLMNOPQRSTUVWY"
            i = i+1
            email += String(Array(alphabet)[Int.random(in: 0..<alphabet.count)])
        }
        print(email)
        return email+"@gmail.com"
    }
}


