//
//  TaskMaster.swift
//  fimmigrant
//
//  Created by iosdev on 26/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import CoreData

class TaskMaster {
    

    // init CoreData class
    // get task data from core data
    private init() {
        
    }
    
    // init the collection of tasks from server and save it in core data
    // when user open the app
    static func initTask() {
        print("get tasks from core data")
        var taskList = [Task]()
        // getting task data from core data
        let fetchRequest: NSFetchRequest<Taskdb> = Taskdb.fetchRequest()
        do {
            let taskListFromCore = try CoreDataHandler.context.fetch(fetchRequest)
            for each in taskListFromCore {
                guard let title = each.title else {
                    return
                }
                guard let link = each.title else {
                    return
                }
                guard let desc = each.descriptions else {
                    return
                }
                guard let preq = each.prerequisite else {
                    return
                }
                var steps = [Step]()
                if let stepsFromCore = each.contain {
                    for each in stepsFromCore {
                        steps.append(Step(whatToDo: each.descriptions ?? "Dont do anything"))
                    }
                }
                let task = Task(title: title, steps: steps, description: desc, link: link, preq: preq)
                taskList.append(task)
            }
        } catch {
            print("init task list failed")
        }

        // getting task data from server
        guard let url = URL(string:"http://media.mw.metropolia.fi/wbma/tags/taskforfimmigrant") else {
            fatalError("fail to create url")
        }

        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        // set up the connection task
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            if let error = error {
                print("Client error \(error)")
            }
            if let data = data, let _ = String(data: data, encoding: .utf8) {
                DispatchQueue.main.async {
                    if let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [[String: Any]] {
                        for each in json {
                            // check if any task of the same name already exist
                            var duplicate = false
                            let title = each["title"] as! String
                            for task in taskList{
                                if (task.title == title) {
                                    duplicate = true
                                }
                            }
                            if (duplicate == false) {
                                let taskDB = Taskdb(context: CoreDataHandler.context)
                                taskDB.title = title
                                let descx = each["description"] as! String
                                let taskData = descx.components(separatedBy: "</()|/+>")
                                taskDB.descriptions = taskData[0]
                                taskDB.link = taskData[1]
                                taskDB.prerequisite = taskData[2]
                                for i in 3..<taskData.count {
                                    let stepDB = Stepdb(context: CoreDataHandler.context)
                                    stepDB.descriptions = taskData[i]
                                    stepDB.status = 0
                                    taskDB.addToContain(stepDB)
                                }
                            }
                            CoreDataHandler.saveContext()
                        }
                    }
                    //TaskMaster.getTaskList()
                }
            }
        }
        task.resume()
    }
    
    // return array of taskDB
    static func getTaskDBByName(title: String?) -> Taskdb? {
        guard let unwrappedTitle = title else {
            return nil
        }
        let fetchRequest: NSFetchRequest<Taskdb> = Taskdb.fetchRequest()
        do {
            fetchRequest.predicate = NSPredicate(format: "title == %@", unwrappedTitle)
            let taskListFromCore = try CoreDataHandler.context.fetch(fetchRequest)
            return taskListFromCore.first
        } catch {
            print("init task list failed")
        }
        return nil
    }
    
    // return array of task
    static func getTaskList() -> [Task] {
        var taskList = [Task]()
        // getting task data from core data
        let fetchRequest: NSFetchRequest<Taskdb> = Taskdb.fetchRequest()
        do {
            let taskListFromCore = try CoreDataHandler.context.fetch(fetchRequest)
            for each in taskListFromCore {
                guard let title = each.title else {
                    return taskList
                }
                guard let link = each.link else {
                    return taskList
                }
                guard let desc = each.descriptions else {
                    return taskList
                }
                guard let preq = each.prerequisite else {
                    return taskList
                }
                var steps = [Step]()
                if let stepsFromCore = each.contain {
                    for each in stepsFromCore {
                        steps.append(Step(whatToDo: each.descriptions ?? "Dont do anything"))
                    }
                }
                let task = Task(title: title, steps: steps, description: desc, link: link, preq: preq)
                taskList.append(task)
            }
        } catch {
            print("init task list failed")
        }
        return taskList
    }
    
    // return user current task list
    static func getMyTaskList() -> [Task] {
        var myTaskList = [Task]()
        let generalTaskList = self.getTaskList()
        
        //
        let fetchRequest: NSFetchRequest<Userdb> = Userdb.fetchRequest()
        do {
            let userdbList = try CoreDataHandler.context.fetch(fetchRequest)
            if(userdbList.isEmpty) {
                return myTaskList
            }
            let user = userdbList[0]
            if let userdbTask = user.has {
                for each in userdbTask {
                    if let title = each.title {
                        for task in generalTaskList {
                            if title == task.title {
                                if each.finis == true {
                                    task.vanillaSetDone()
                                }
                                myTaskList.append(task)
                            }
                        }
                    }
                }
            }
        } catch {
            print("somethingWentWrong")
        }
        return myTaskList
    }
    
    // set tasks in core data as done
    static func setDone(task: Task) {
        if let taskDB = getTaskDBByName(title: task.title) {
            taskDB.setDone()
            CoreDataHandler.saveContext()
        }
    }
}
