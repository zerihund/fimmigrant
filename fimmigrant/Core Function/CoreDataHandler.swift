
import Foundation
import CoreData

class CoreDataHandler {
    
    // private init allows you to call the functions inside core data handler by these syntaxes:
    // CoreDataHandler.getData()
    private init(){
        
    }
    
    // add default value to users. User's properties are to be encoded into a string
    // and saved on server as in the full_name properties
    // and log users in after they sign up so users can start using the app immediately without
    // going thru the log in process themselves
    static func firstTimeSignUpInit(name: String, password: String, view: SignUpController) {
        guard let url = URL(string:"http://media.mw.metropolia.fi/wbma/login") else {
            fatalError("fail to create url")
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: ["username": name, "password": password], options: .prettyPrinted)
        } catch let error {
            print("error is here")
            print(error.localizedDescription)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // set up the connection task
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            if let error = error {
                print("Client error \(error)")
            }
            
            guard let httpResponse = response as? HTTPURLResponse,(200...299).contains(httpResponse.statusCode) else {
                return
            }
            if let data = data, let _ = String(data: data, encoding: .utf8) {
                DispatchQueue.main.async {
                    let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    // print(json!)
                    // save user data into core data so the data can be used across the map without
                    // making too many API calls
                    let user = Userdb(context: self.context)
                    let userInfo = json?["user"] as! [String: Any]
                    user.username = userInfo["username"] as! String?
                    user.token = json?["token"] as! String?
                    user.experience = 0
                    user.lastupdate = 0
                    // randomize the user avatar
                    let x = "av" + String(Int.random(in: 1..<9))
                    user.avatar = x
                    
                    // initialize the string that encode the user data onto server
                    // the string is going the decoded when received back from server
                    // through the use of array split functions (or iOs equivalent)
                    // the order of the information:
                    // 1. last update: This is to check which version of the data is latest
                    // so we can know which data needs to be changed to the newest version
                    // 2. user experience point
                    // 3. number of task currently on user list followed by names of tasks
                    // 4. number of task users have already finished followed by names of finished tasks
                    // 5. number of achievements followed by name of achievements done by users
                    // 6. user avatar
                    var outputToServer = String(user.lastupdate) + "/()|/" + String(user.experience) + "/()|/" + "0" + "/()|/" + "0" + "/()|/" + "0" + "/()|/"
                    outputToServer += x
                    print(outputToServer)
                    self.putUserDataOnServer(output: outputToServer, tk: user.token, view: view)
                    self.saveContext()
                }
            }
        }
        task.resume()
    }
    
    // init user data on server
    static func putUserDataOnServer(output: String, tk: String?, view: SignUpController?) {
        print("init user data")
        guard let url = URL(string:"http://media.mw.metropolia.fi/wbma/users") else {
            fatalError("fail to create url")
        }
        guard let token = tk else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: ["full_name": output], options: .prettyPrinted)
        } catch let error {
            print("error is here")
            print(error.localizedDescription)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(token, forHTTPHeaderField: "x-access-token")
        
        // set up the connection task
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            if let error = error {
                print("Client error \(error)")
            }
            
            if let data = data, let _ = String(data: data, encoding: .utf8) {
                DispatchQueue.main.async {
                    let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    print(json ?? "no data sent back")
                    if let signUpView = view {
                        signUpView.logIn()
                    }
                }
            }
        }
        task.resume()
    }
    
    // fetch user data and data from the server
    // then passes it to another functions to compare the versions and their contents
    static func checkData(token: String?, logInView: LoginViewController?, signUpView: SignUpController?, profileView: ProfileViewController?) {
        guard let tk = token else {
            return
        }
        print("coming from coredatahandler")
        print(tk)
        // get user data from server
        guard let url = URL(string:"http://media.mw.metropolia.fi/wbma/users/user") else {
            fatalError("fail to create url")
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue(tk, forHTTPHeaderField: "x-access-token")
        
        // set up the connection task
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            if let error = error {
                print("Client error \(error)")
            }
            if let data = data, let _ = String(data: data, encoding: .utf8) {
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                print(json!)
                // get information from server
                if let serverUserData = json?["full_name"] as! String?{
                    if let name = json?["username"] as! String? {
                        let serverData = serverUserData.components(separatedBy: "/()|/")
                        let fetchRequest: NSFetchRequest<Userdb> = Userdb.fetchRequest()
                        do {
                            let userdbList = try self.context.fetch(fetchRequest)
                            print(userdbList)
                            if(userdbList.isEmpty) {
                                let user = Userdb(context: self.context)
                                user.lastupdate = -1
                                self.performUpdateData(user: user, data: serverData, tk: tk, name: name, logInView: logInView, signUpView: signUpView, profileView: profileView)
                            } else {
                                let user = userdbList[0]
                                self.performUpdateData(user: user, data: serverData, tk: tk, name: name,logInView: logInView, signUpView: signUpView, profileView: profileView)
                            }
                        } catch {
                            print("somethingWentWrong")
                        }
                    }
                    
                }
            }
        }
        task.resume()
    }
    
    // update data either on server or in core data, depends on the header of the data string
    static func performUpdateData(user: Userdb, data: [String], tk: String, name: String, logInView: LoginViewController?, signUpView: SignUpController?, profileView: ProfileViewController?) {
        guard let serverLastUpdate = Int16(data[0]) else {
            return
        }
        print(user.lastupdate)
        print(serverLastUpdate)
        // initialize the string that encode the user data onto server
        // the string is going the decoded when received back from server
        // through the use of array split functions (or iOs equivalent)
        // the order of the information:
        // 1. last update: This is to check which version of the data is latest
        // so we can know which data needs to be changed to the newest version
        // 2. user experience point
        // 3. number of task currently on user list followed by names of tasks
        // 4. number of task users have already finished followed by names of finished tasks
        // 5. number of achievements followed by name of achievements done by users
        // 6. user avatar
        if (user.lastupdate > serverLastUpdate) {
            print("it did someething")
             //add last update to head of string
            var newData = String(user.lastupdate) + "/()|/"
            // add experience to string
            newData += String(user.experience) + "/()|/"
            // add number of task and task name
            if let tasks = user.has {
                print(tasks)
                if (tasks.count > 0) {
                    newData += String(tasks.count) + "/()|/"
                    var finis = ""
                    var noFinis = 0
                    for each in tasks {
                        if let title = each.title {
                            newData += title + "/()|/"
                        }
                        if each.finis == true {
                            print("=================")
                            print(each.title! + " is finis")
                            noFinis += 1
                            finis += each.title ?? ""
                            finis += "/()|/"
                        }
                    }
                    newData += String(noFinis) + "/()|/" + finis
                } else {
                    newData += String(0) + "/()|/" + String(0) + "/()|/"
                }
            } else {
                newData += String(0) + "/()|/" + String(0) + "/()|/"
            }
            print("data string b4 achiv add xxxxxxxxxxxxxxxxxxxxxxxxx")
            print(newData)
            // add achievement count
            if let achivdbList = user.achieve {
                print("achivListååååååååååååååååååååååå")
                print(achivdbList.count)
                if (achivdbList.count == 0) {
                    newData += String(0) + "/()|/"
                } else {
                    newData += String(achivdbList.count) + "/()|/"
                    for each in achivdbList {
                        if let name = each.name {
                            newData += name + "/()|/"
                        }
                    }
                }
            }
            
            newData += user.avatar ?? "av8"
            putUserDataOnServer(output: newData, tk: tk, view: nil)
            
        } else if (user.lastupdate < serverLastUpdate) {
            user.username = name
            user.lastupdate = serverLastUpdate
            user.token = tk
            user.experience = Int16(data[1]) ?? 0

            // the index of different data from the server
            var doneIndex: Int = 3
            var achivIndex: Int = 4
            var avatarIndex: Int = 5
            // add user task from server to user core data
            if let noUserTask = Int16(data[2]) {
                if noUserTask > 0 {
                    var i = 3
                    while( i < noUserTask + 3 ) {
                        var duplicate = false
                        if let userTask = user.has {
                            for each in userTask {
                                if let title = each.title {
                                    if title == data[i] {
                                        duplicate = true
                                    }
                                }
                            }
                        }
                        if let task = TaskMaster.getTaskDBByName(title: data[i]) {
                            if duplicate == false {
                                user.addToHas(task)
                            }
                        }
                        i = i+1
                    }
                }
                // moving the index up when iterate through the data array, in this case, move the index up
                // in accordance to the number of task in the array
                achivIndex += Int(noUserTask)
                doneIndex += Int(noUserTask)
                avatarIndex += Int(noUserTask)
            }
            print("number of done index: " + String(doneIndex))
             //update list of finished tasks
            if let noDoneTask = Int(data[doneIndex]) {
                var i = 1 + doneIndex
                while ( i <= noDoneTask + doneIndex) {
                    if let userTask = user.has {
                        for each in userTask {
                            if let title = each.title {
                                if title == data[i] {
                                    each.setDone()
                                }
                            }
                        }
                    }
                    i += 1
                }
                // move the index up according to the number of task already done
                achivIndex += noDoneTask
                avatarIndex += noDoneTask
            }
            
            //  add achievement to user
            if let noAchiv = Int(data[achivIndex]) {
                print("number of achiv: " + String(noAchiv))
                if (noAchiv == 0) {
                    print("no achiv yet")
                } else {
                    var index = achivIndex + 1
                    while (index <= noAchiv + achivIndex) {
                        var dup = false
                        // chec
                        if let achivlist = user.achieve {
                            for each in achivlist {
                                if let name = each.name {
                                    if name == data[index] {
                                        dup = true
                                    }
                                }
                            }
                            
                            if dup == false {
                                if let achivdb = AchivManager.getAchivDBByName(title: data[index]) {
                                    user.addToAchieve(achivdb)
                                }
                            }
                        }
                        index += 1
                    }
                }
                // avatar index is at the end of the string, so it needs to be moved up according to the number
                // of achievements
                avatarIndex += noAchiv
            }
            // set up user avatar
            user.avatar = data[avatarIndex]
            print("index of avatar is: " + String(avatarIndex))
            self.saveContext()
        } else {
            print("no difference. safe to proceed")
        }
        DispatchQueue.main.async {
            if let view = logInView {
                // log in after checking user data when loggin in
                view.logIn()
            }
            
            if let view = signUpView {
                // log in after init user data after sign up
                view.logIn()
            }
            
            if let view = profileView {
                // log user out after checking user data in core n in server
                view.trueLogOut()
            }
        }
    }
    
    static var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    // MARK: - Core Data stack
    static var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "fimmigrant")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    static func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // log out means erasing all user data in core data, also task data and achievement data
    // so when the user log in again, they can fetch new data from the server
    static func logOut() {
        print("prepping for logging out")
        
        // delete user core data
        var deleteRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Userdb")
        var deleteAll = NSBatchDeleteRequest(fetchRequest: deleteRequest)
        do {
            try self.context.execute(deleteAll)
        } catch {
            print(error)
        }
        
        // delete task core data
        deleteRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Taskdb")
        deleteAll = NSBatchDeleteRequest(fetchRequest: deleteRequest)
        do {
            try self.context.execute(deleteAll)
        } catch {
            print(error)
        }
        
        // delete task core data
        deleteRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Stepdb")
        deleteAll = NSBatchDeleteRequest(fetchRequest: deleteRequest)
        do {
            try self.context.execute(deleteAll)
        } catch {
            print(error)
        }
        
        // delete task core data
        deleteRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Achievementdb")
        deleteAll = NSBatchDeleteRequest(fetchRequest: deleteRequest)
        do {
            try self.context.execute(deleteAll)
        } catch {
            print(error)
        }
    }
}
