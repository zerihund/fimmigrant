//
//  UserManager.swift
//  fimmigrant
//
//  Created by iosdev on 29/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import CoreData

// this class handles thing concerning user
class UserManager {
    private init() {}
    
    // auto log in the user in if the token is valid
    static func autoLogin (view: LoginViewController) {
        let fetchRequest: NSFetchRequest<Userdb> = Userdb.fetchRequest()
        do {
            print("init trying to auto log in")
            let userdbList = try CoreDataHandler.context.fetch(fetchRequest)
            if userdbList.isEmpty {
                return
            }
            let user = userdbList[0]
            if let token = user.token {
                print(token)
                // check if token is still use-able
                guard let url = URL(string:"http://media.mw.metropolia.fi/wbma/users/user") else {
                    fatalError("fail to create url")
                }
                
                // send token to server to check for token validity
                var request = URLRequest(url: url)
                request.httpMethod = "GET"
                request.addValue(token, forHTTPHeaderField: "x-access-token")
                
                // set up the connection task
                let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                    if let error = error {
                        print("Client error \(error)")
                    }
                    
                    guard let httpResponse = response as? HTTPURLResponse,(200...299).contains(httpResponse.statusCode) else {
                        return
                    }
                    DispatchQueue.main.async {
                        CoreDataHandler.checkData(token: user.token, logInView: nil, signUpView: nil, profileView:  nil)
                        view.autoLogin()
                    }
                }
                task.resume()
            }
        } catch {
            print("somethingWentWrong")
        }
    }
    
    // return the user and all the user properties, this will be called by the AppDelegate to avoid having duplicates
    // of the same user class
    static func getUser() -> User? {
        let fetchRequest: NSFetchRequest<Userdb> = Userdb.fetchRequest()
        do {
            let userdbList = try CoreDataHandler.context.fetch(fetchRequest)
            if(userdbList.isEmpty) {
                return nil
            }
            print(userdbList)
            let userdb = userdbList[0]
            if let usrname = userdb.username {
                if let token = userdb.token {
                    print("fetching user from core data")
                    // setting up the user properties
                    let user = User(name: usrname, tk: token)
                    let _ = user.gainExperience(exp: Int(userdb.experience))
                    user.setAvatarDataURL(url: userdb.avatar ?? "avt8")
                    // get user tasklist from core data, which is fetched from the server and loaded in
                    // when the app is turned on
                    let myTaskList = TaskMaster.getMyTaskList()
                    for each in myTaskList {
                        user.addTask(task: each)
                    }
                    // get user achievement list from core data, fetched the same way as tasks
                    if let achivList = userdb.achieve {
                        for each in achivList {
                            if let title = each.name {
                                if let achiv = AchivManager.getAchivByName(title: title) {
                                    user.addAchievements(achiv: achiv)
                                }
                            }
                        }
                    }
                    return user
                }
            }
        } catch {
            print("somethingWentWrong")
        }
        return nil
    }
    
    // get user db from core data
    static func getUserDB() -> Userdb? {
        let fetchRequest: NSFetchRequest<Userdb> = Userdb.fetchRequest()
        do {
            let userdbList = try CoreDataHandler.context.fetch(fetchRequest)
            if userdbList.isEmpty {
                return nil
            }
            return userdbList[0]
        } catch {
            print("somethingWentWrong")
        }
        return nil
    }
    
    // add taskdb to userdb and update it in core data, so the data will be saved in core data
    static func addToUserTaskInCoreData(task: Task) {
        if let taskdb = TaskMaster.getTaskDBByName(title: task.title) {
            if let user = UserManager.getUserDB() {
                user.lastupdate += 1
                user.addToHas(taskdb)
                CoreDataHandler.saveContext()
            }
        }
    }
    
    // remove taskdb from userdb and update it in core data
    static func removeUserTaskInCoreData(task: Task) {
        if let taskdb = TaskMaster.getTaskDBByName(title: task.title) {
            if let user = UserManager.getUserDB() {
                user.lastupdate += 1
                user.removeFromHas(taskdb)
                CoreDataHandler.saveContext()
            }
        }
    }
    
    // update user experience in core data
    static func setUserExpInCoreData(exp: Int) {
        if let user = UserManager.getUserDB() {
            user.lastupdate += 1
            user.experience = Int16(exp)
            CoreDataHandler.saveContext()
        }
    }
    
    // update user avatar in core data
    static func setUserAvatarInCoreData(img: String) {
        if let user = UserManager.getUserDB() {
            user.lastupdate += 1
            user.avatar = img
            CoreDataHandler.saveContext()
        }
    }
    
    // update user name in core data
    static func setUserNameInCoreData(name: String) {
        if let user = UserManager.getUserDB() {
            user.lastupdate += 1
            user.username = name
            CoreDataHandler.saveContext()
        }
    }
}
