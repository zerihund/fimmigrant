
import Foundation
import CoreData

class AchivManager {
    private init() {}
    
    // initiate the achievements when the app is on
    // typically achievements should be handled by server, not in-app like this to allow for flexible
    // achievements, but due to the server's limited capacity, this will have to suffice
    static func achivInit() {
        let signUpAchiv = Achievement(name: "Newcomer", exp: 500, img: "lvl1", des: "Welcome to Finland")
        let firstTaskAchiv = Achievement(name: "First step", exp: 500, img: "lvl2", des: "Complete your first task")
        let wishAchiv = Achievement(name: "Wishmaster", exp: 500, img: "lvl3", des: "Added five tasks to your list")
        let achieverAchiv = Achievement(name: "Achiever", exp: 500, img: "lvl4", des: "Complete five task")
        let recurAchiv = Achievement(name: "Welcome Back", exp: 500, img: "lvl5", des: "Log in again")
        let achivArray = [signUpAchiv, firstTaskAchiv, wishAchiv, achieverAchiv, recurAchiv]
        
        // put achiv in core data after initiation
        for each in achivArray {
            let achivDB = Achievementdb(context: CoreDataHandler.context)
            achivDB.name = each.name
            achivDB.desc = each.desc
            achivDB.exp = Int32(each.expValue)
            achivDB.img = each.image
            CoreDataHandler.saveContext()
        }
    }
    
    // get achiv db by name, which is used for modification later
    static func getAchivDBByName(title: String?) -> Achievementdb? {
        guard let unwrappedTitle = title else {
            return nil
        }
        let fetchRequest: NSFetchRequest<Achievementdb> = Achievementdb.fetchRequest()
        do {
            fetchRequest.predicate = NSPredicate(format: "name == %@", unwrappedTitle)
            let achivListFromCore = try CoreDataHandler.context.fetch(fetchRequest)
            return achivListFromCore.first
        } catch {
            print("name not found")
        }
        return nil
    }
    
    // get achiv by name
    static func getAchivByName(title: String?) -> Achievement? {
        if let achivdb = self.getAchivDBByName(title: title) {
            guard let name = achivdb.name, let desc = achivdb.desc, let img = achivdb.img else {
                return nil
            }
            return Achievement(name: name, exp: Int(achivdb.exp), img: img, des: desc)
        }
        return nil
    }
    
    // add achiv to user core data
    static func addAchivToCoreData(achiv: Achievement) {
        let fetchRequest: NSFetchRequest<Userdb> = Userdb.fetchRequest()
        do {
            let userdbList = try CoreDataHandler.context.fetch(fetchRequest)
            if (userdbList.isEmpty) {
                print("no user in core")
                return
            }
            let userdb = userdbList[0]
            if let achivdb = self.getAchivDBByName(title: achiv.name) {
                userdb.addToAchieve(achivdb)
                userdb.lastupdate += 1
                CoreDataHandler.saveContext()
            }
        } catch {
            print("no user found in core")
        }
    }
    
    // get achiv lists, this is exactly the same in the init
    static func getAchivList() -> [Achievement] {
        let signUpAchiv = Achievement(name: "Newcomer", exp: 500, img: "lvl1", des: "Welcome to Finland")
        let firstTaskAchiv = Achievement(name: "First step", exp: 500, img: "lvl2", des: "Complete your first task")
        let wishAchiv = Achievement(name: "Wishmaster", exp: 500, img: "lvl3", des: "Added five tasks to your list")
        let achieverAchiv = Achievement(name: "Achiever", exp: 500, img: "lvl4", des: "Complete five task")
        let recurAchiv = Achievement(name: "Welcome Back", exp: 500, img: "lvl5", des: "Log in again")
        return [signUpAchiv, firstTaskAchiv, wishAchiv, achieverAchiv, recurAchiv]
    }
    
    // check if users have attained any achiv and return list of unattained achivs
    static func getUnattainedAchiv(user: User) -> [Achievement] {
        let list = self.getAchivList()
        var unattainedList = [Achievement]()
        print("checking current user achievements")
        for each in user.getAchievements() {
            print(each.name)
        }
        print("-----------------------------------------")
        if (user.getAchievements().isEmpty) {
            unattainedList = list
        } else {
            for each in list {
                // checking if user has certain achievements yet
                var dup = false
                for item in user.getAchievements() {
                    if each.name == item.name {
                        dup = true
                    }
                }
                if (dup == false) {
                    unattainedList.append(each)
                }
            }
        }
        return unattainedList
    }
    
    // check if conditions in unattained achivs are met and add them to user achievements
    static func checkAchivCon(user: User, checkAchiv: String) {
        print("b4: ------------------------------------------")
        print(user.getAchievements())
        let list = self.getUnattainedAchiv(user: user)
        for each in list {
            print(each.name)
            switch each.name {
            // finish the sign up sequence
            case "Newcomer":
                if (checkAchiv == "new") {
                    let _ = user.gainExperience(exp: each.expValue)
                    self.addAchivToCoreData(achiv: each)
                    user.addAchievements(achiv: each)
                    print("add new come")
                }
                
            // finish first task
            case "First step":
                if (user.checkTaskCompletion() == 1) {
                    let _ = user.gainExperience(exp: each.expValue)
                    self.addAchivToCoreData(achiv: each)
                    user.addAchievements(achiv: each)
                    print("add first step")
                }
                
            // added 5 tasks to list
            case "Wishmaster":
                if (user.taskList.count == 5) {
                    let _ = user.gainExperience(exp: each.expValue)
                    self.addAchivToCoreData(achiv: each)
                    user.addAchievements(achiv: each)
                    print("add wish master")
                }
                
            // complete 5 tasks
            case "Achiever":
                if (user.checkTaskCompletion() == 5) {

                    let _ = user.gainExperience(exp: each.expValue)
                    self.addAchivToCoreData(achiv: each)
                    user.addAchievements(achiv: each)
                    print("add achiever")
                }
                
            // log in after log out
            case "Welcome Back":
                if (checkAchiv == "back") {
                    let _ = user.gainExperience(exp: each.expValue)
                    self.addAchivToCoreData(achiv: each)
                    user.addAchievements(achiv: each)
                    print("add welcome")
                }
                
            default:
                print("")
            }
        }
        print("after: ------------------------------------------")
        for each in user.getAchievements(){
            print(each.name)
        }
    }
}
