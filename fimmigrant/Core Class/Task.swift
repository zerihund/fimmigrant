
import Foundation
import UIKit


class Task: NSObject, Comparable {
    static func < (lhs: Task, rhs: Task) -> Bool {
        return true
    }
    
    static func == (lhs: Task, rhs: Task) -> Bool {
        return lhs.title == rhs.title
    }
    
    let title: String
    private(set) var taskComplete = false
    private(set) var desc: String
    private(set) var link: String
    private(set) var prerequisite: String
    private(set) var taskImage: UIImage?
    
    //TODO: Uncomment
    var stepArray: [Step]
    
    init(title: String, steps: [Step], description: String, link: String, preq: String){
        self.title = title
        self.stepArray = steps
        self.desc = description
        self.link = link
        self.prerequisite = preq
    }
    
     func doneStepsCounter() -> Int {
        let amount = stepArray.reduce(0) {(accumulator, next) -> Int in
            let ans = accumulator + next.getDone()
            return ans
        }
        return amount
    }
    
    private var progressPercentage: Double {
        //completion degree: Int (out of 100% or out of 5 or 10)
        //OR  steps done basically a numerical value
        return Double( doneStepsCounter() / stepArray.count) * 100
    }
     
    func setStepDone(theStep: Step) -> Step {
        let _ = theStep.setDone(state: true)
        return theStep
    }
    //This is new. The task image
    func setTaskImage(image: UIImage) {
        self.taskImage = image
    }
    
    func setDone() {
        TaskMaster.setDone(task: self)
        self.taskComplete = true
    }
    
    func vanillaSetDone() {
        self.taskComplete = true
    }
}

