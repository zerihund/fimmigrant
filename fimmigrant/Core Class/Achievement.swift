class Achievement {
    
    let name: String
    let expValue: Int
    let image: String
    let desc: String
    
    init(name: String, exp: Int, img: String, des: String) {
        self.name = name
        self.expValue = exp
        self.image = img
        self.desc = des
    }
}
