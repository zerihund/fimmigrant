import Foundation

class User {
    private var name: String
    let token: String
    private(set) var experience: Int = 0
    private let maxExp = 6000
    private var achievements: [Achievement] //list of achievement
    private var avatarURL: String?
    private var avatarData: Data?
    private(set) var taskList: [Task]
    private var completeTaskList: [Task] {
        return taskList.filter({item -> Bool in
            return item.taskComplete
        })
    }
    
    init(name: String, tk: String) {
        self.name = name
        self.token = tk
        self.experience = 0;
        self.achievements = [Achievement]()
        self.avatarURL = nil
        self.avatarData = nil
        self.taskList = [Task]()
    }
    
    // return false if user reached max level and cannot gain any level anymore
    func gainExperience(exp: Int) -> Bool {
        if (self.experience + exp >= self.maxExp) {
            self.experience = self.maxExp
            UserManager.setUserExpInCoreData(exp: self.experience)
            return false
        } else {
            self.experience += exp
            UserManager.setUserExpInCoreData(exp: self.experience)
            return true
        }
    }
    
    func getAchievements() -> [Achievement] {
        return self.achievements
    }
    
    // add achievement
    func addAchievements(achiv: Achievement) {
        self.achievements.append(achiv)
    }
    
    // return nil if user does not have any data yet
    func getAvatarData() -> Data? {
        return self.avatarData
    }
    
    // add userImage Data
    func setAvatarData(imgData: Data) {
        self.avatarData = imgData
    }
    
    // return nil if user does not have any data yet
    func getAvatarDataURL() -> String? {
        return self.avatarURL
    }
    
    // add userImage URL
    func setAvatarDataURL(url: String) {
        UserManager.setUserAvatarInCoreData(img: url)
        self.avatarURL = url
    }
    
    // return user name
    func getUserName() -> String {
        return self.name
    }
    
    // return the new user name
    func setName(newName: String) -> String {
        self.name = newName
        return self.name
    }
    
    // add task to user task list
    func addTask(task: Task) {
        for each in self.taskList {
            if each.title == task.title {
                return
            }
        }
        print("task added:" + task.title)
        UserManager.addToUserTaskInCoreData(task: task)
        self.taskList.append(task)
    }
    
    func removeTask(task: Task) {
        print("task removed:" + task.title)
        UserManager.removeUserTaskInCoreData(task: task)
        for i in 0..<self.taskList.count {
            if (task.title == self.taskList[i].title) {
                self.taskList.remove(at: i)
            }
        }
    }
    
    // get level of user
    func getLevel() -> Int {
        let level: Int = (self.experience/1000)
        return level
    }
    // return number of task user has complete
    func checkTaskCompletion() -> Int {
        return completeTaskList.count
    }
    
    func getTaskList () -> [Task] {
        return self.taskList
    }
}
