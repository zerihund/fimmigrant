//
//  Step.swift
//  fimmigrant
//
//  Created by iosdev on 26/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

class Step: NSObject {
    private(set) var instruction: String
    private var done = false
    
    init(whatToDo: String) {
        self.instruction = whatToDo
        self.done = false
    }
    
    func getDone() -> Int {
        if done{
            return 1
        }else {
            return 0
        }
    }
    
    func setDone(state: Bool) -> Bool {
        self.done = state
        return self.done
    }
}
