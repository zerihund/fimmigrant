//
//  fimmigrantUITests.swift
//  fimmigrantUITests
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest

class fimmigrantUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        XCUIApplication().terminate()

    }
    func login() {
        let app = XCUIApplication()
        let userField = app.textFields["Username"]
        userField.tap()
        userField.typeText("wsx")
        let passwordSecureTextField = app.secureTextFields["Password"]
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText("12345")
        app.buttons["Login"].tap()
        let myTaskElement = app.navigationBars["My Tasks"].otherElements["My Tasks"]
        XCTAssert(myTaskElement.exists)
    }
    
    func testaLoginValidSuccess(){
        // Check for valid Login
        let app = XCUIApplication()
        let userField = app.textFields["Username"]
        userField.tap()
        userField.typeText("wsx")
        let passwordSecureTextField = app.secureTextFields["Password"]
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText("12345")
        app.buttons["Login"].tap()
        let myTaskElement = app.navigationBars["My Tasks"].otherElements["My Tasks"]
        XCTAssert(myTaskElement.exists)
    }
    func testThreeTabs(){
        // check the tabs if the tabs function accordingly
        let tabBarsQuery = XCUIApplication().tabBars
        tabBarsQuery.buttons["Tasks"].tap()
        tabBarsQuery.buttons["Profile"].tap()
        let myTask = tabBarsQuery.buttons["My Tasks"]
        XCTAssert(myTask.exists)
    }
    
    func testTaskDoneValid(){
        //Test task completed button
        
        let app = XCUIApplication()
        app.tables.staticTexts["Occupational Safety Card"].tap()
        let finishButton = app.buttons["Finish Task"]
        XCTAssertFalse(finishButton.exists)
    }
    
    func testLinkAction()  {
        // check link action to open the web view
        let app = XCUIApplication()
        app.tables.staticTexts["HSL Card"].tap()
        let elementsQuery = app.scrollViews.otherElements
        elementsQuery.staticTexts["Application  Steps:"].swipeUp()
        elementsQuery.staticTexts["https://www.hsl.fi/en/information/hsl-card"].tap()
        
    }
    func testZLogoutSuccessful(){
        // check the logout button when the user confirms logout
        let app = XCUIApplication()
        app.tabBars.buttons["Profile"].tap()
        app.navigationBars["Profile"].buttons["Logout"].tap()
        app.buttons["YES"].tap()
        
    }
    func testLogoutCanceled(){
        //Check the logout button when the user cancel logout
        let app = XCUIApplication()
        app.tabBars.buttons["Profile"].tap()
        app.navigationBars["Profile"].buttons["Logout"].tap()
        let cancelButton = app.buttons["NO"]
        XCTAssert(cancelButton.exists)
    }
    func  testBackMytaskDetail(){
        // back button from mytask detailview
        let app = XCUIApplication()
        app.tables.staticTexts["Hygiene Passport"].tap()
        app.navigationBars["fimmigrant.TaskDetailView"].buttons["Back"].tap()
        let myTask = app.navigationBars["My Tasks"].staticTexts["My Tasks"]
        XCTAssert(myTask.exists)
        
    }
    func testBackTaskDetail(){
        //back button from task detailview
        
        let app = XCUIApplication()
        app.tabBars.buttons["Tasks"].tap()
        app.tables.staticTexts["General Housing Allowance"].tap()
        app.navigationBars["fimmigrant.GeneralTaskView"].buttons["Back"].tap()
        let allTask = app.navigationBars["All Tasks"].staticTexts["All Tasks"]
        XCTAssert(allTask.exists)
    }
    func testWebviewBackForAlltask(){
        //back button form the web view
        let app = XCUIApplication()
        app.tabBars.buttons["Tasks"].tap()
        app.tables.staticTexts["Finnish identity card"].tap()
        let elementsQuery = app.scrollViews.otherElements
        elementsQuery.staticTexts["https://www.poliisi.fi/applying_for_an_identity_card"].tap()
        app.navigationBars["fimmigrant.AllWebView"].buttons["Back"].tap()
        
    }
    func testWebviewBackForMytask()  {
        // back buton fuction from the web view of all task page
        let app = XCUIApplication()
        app.tables.staticTexts["HSL Card"].tap()
        
        let scrollViewsQuery = app.scrollViews
        scrollViewsQuery.otherElements.containing(.staticText, identifier:"HSL Card").element.swipeLeft()
        scrollViewsQuery.otherElements.staticTexts["https://www.hsl.fi/en/information/hsl-card"].tap()
        app.navigationBars["fimmigrant.WebView"].buttons["Back"].tap()
        
    }
    func testUserProfile(){
        // test the profile page contains level and achievements
        let app = XCUIApplication()
        app.tabBars.buttons["Profile"].tap()
        let levelText = app.staticTexts["200 left to next level"]
        XCTAssert(levelText.exists)
        let level = app.staticTexts["Level 0"]
        XCTAssert(level.exists)
        let achivementsLabel = app.staticTexts["Achievements"]
        XCTAssert(achivementsLabel.exists)
        
    }
    
}
