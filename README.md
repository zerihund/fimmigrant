# Fimmigrant
<h2>Project: Finnish immigrants assistance app</h2>
<p>08 May 2019</p>

<p>implemented using <b>Swift,xCode & iOS</b></p>
<h4>Continuous Integration CI/CD</h4>


<h3>What does it do?</h3>
<p>Our idea was to make an application that helps Immigrants with migration to Finland or integration into Finnish society. <br>
Our app acts a checklist of important things that you need in finland, such as kela card or bus card,from which you can<br>
select the tasks that are relevant to you take care of. It is structured such that you can login on any phone and<br> 
continue your progress. We gamified the process by adding achievements, experience points and levels.</p>


<h4>Required Functionalities</h4>
<ul>
<li>Unit and UI Testing</li>
<li>Delegation, Object-oriented, well implemented data structures and design patterns</li>
<li>Commented code</li>
<li>Code is in shared repository in gitlab.com</li>
<li>CI pipeline is up and running for the project.</li>
<li>Language localisation</li>
<li>Uses the Core Data framework</li>
<li>At least one back end API</li>
<li>Application either helps immigrants find information/education/job</li>
</ul>

<h4>Extra Functionalities</h4>
<ul>
<li>Data saved remotely; can be used from any phone</li>
<li>App can be logged into on phone due to remote storage</li>
<li>online Api</li>
<li>Gamification</li>
<li>Achievements</li>
<li>Levels</li>
<li>Experience points system</li>
<li>responsive frontend*</li>
<li>user registration and authentication*</li>
</ul>


<h2>Preview</h2>

| Register a new user  | 
|------------|
| ![registerUser](https://user-images.githubusercontent.com/33485810/58747980-08809880-847b-11e9-8583-fc63ce32255d.gif)|

| Fill questionnaire on what you have and want. <br>-> Personal task list is curated accordingly.  | 
|------------|
| ![questionnaire to results](https://user-images.githubusercontent.com/33485810/58747976-00c0f400-847b-11e9-912a-a0f77a9b963f.gif) |

|  A task's detailview and usage of webview | 
|------------|
| ![taskDetail and Webview](https://user-images.githubusercontent.com/33485810/58747983-0cacb600-847b-11e9-98da-a99fec409cfe.gif) |

| Add a task to my tasks; setting a<br> task to finished adds it to finished tasks  | 
|------------|
| ![addTask and setFinished](https://user-images.githubusercontent.com/33485810/58747970-f7378c00-847a-11e9-96a4-fa38fbc514d2.gif) |

| Achievement is added after <br> completing a task | 
|------------|
| ![getAchievement](https://user-images.githubusercontent.com/33485810/58747974-fbfc4000-847a-11e9-8a18-1fa07b27e9f4.gif) |
